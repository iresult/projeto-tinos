package br.com.iresult.gmfmobile.repository

import android.util.Log
import br.com.iresult.gmfmobile.model.bean.Address
import br.com.iresult.gmfmobile.model.bean.Ligacao
import br.com.iresult.gmfmobile.model.bean.Parametro
import br.com.iresult.gmfmobile.model.bean.Roteiro
import br.com.iresult.gmfmobile.model.dao.AddressDao
import br.com.iresult.gmfmobile.model.dao.LigacaoDao
import br.com.iresult.gmfmobile.model.dao.RoteiroDao
import br.com.iresult.gmfmobile.service.TarefaService
import br.com.iresult.gmfmobile.utils.defaultScheduler
import io.reactivex.Observable
import io.reactivex.Single
import okhttp3.ResponseBody
import org.jetbrains.anko.doAsync

class RoteiroRepository(// private val estatisticaDao: PreenchimentoEstatisticaDao,
                        private val roteiroDao: RoteiroDao,
                        private val addressDao: AddressDao,
                        private val ligacaoDao: LigacaoDao,
                        private val tarefaService: TarefaService) {

//    fun getEstatistica(name: String): Observable<PreenchimentoEstatistica> {
//        return estatisticaDao.getEstatistica(name).defaultScheduler()
//    }

    fun getRoteiro(name: String): Observable<Roteiro> {
        return roteiroDao.getRoteiro(name).defaultScheduler()
    }

    fun getRoteiros(): Observable<List<Roteiro>> {
        return roteiroDao.getRoteiros().defaultScheduler()
    }

    fun insertRuas(addresses: List<Address>) {
        doAsync { addressDao.insertAll(addresses) }
    }

//    fun getEstatisticaByRua(nome: String): Observable<PreenchimentoEstatistica> {
//        return estatisticaDao.getEstatisticaByRua(nome).defaultScheduler()
//    }

    fun fetchImoveis(streetName: String): Observable<List<Ligacao>> {
        return ligacaoDao.searchByEndereco(streetName).defaultScheduler()
    }

    fun allImoveis(): Observable<List<Ligacao>> {
        return ligacaoDao.all().defaultScheduler()
    }

    fun desativarRoteiro(nome : String) {
        doAsync { roteiroDao.updateStatus(nome, 'F') }
    }

    fun finalizarTarefa(nomeArquivo: String, parametros: Parametro, roteiro : Roteiro?, naoRealizados : Int): Single<ResponseBody> {
        val dsLinha = StringBuffer()
        Log.i("FinishTaskViewModel", "Repositorio finalizarTarefaInvocado()")
        dsLinha.append("SALTOWEB.PRC_COLETOR_SIMU_CEL@SALTOWEBH(''ENCERRAR'', '''', ")
        dsLinha.append("''$nomeArquivo'', ")
        dsLinha.append("''LS01PARA0200001'', ")
        dsLinha.append("''${parametros.numReg.padStart(6, '0')}#")
        dsLinha.append("${roteiro?.mes?.substring(3..6)}#")
        dsLinha.append("${roteiro?.mes?.substring(0..1)}#")
        dsLinha.append("${parametros.numConrtole}#")
        dsLinha.append("${parametros.numRota}#")
        dsLinha.append("E#")
        dsLinha.append("$naoRealizados''")
        Log.i("FinishTaskViewModel", "Encaminhando os dados para finalizar a tarefa")
        Log.i("FinishTaskViewModel", "$dsLinha")
        return tarefaService.finalizarTarefa(nomeArquivo, dsLinha.toString())
    }
}