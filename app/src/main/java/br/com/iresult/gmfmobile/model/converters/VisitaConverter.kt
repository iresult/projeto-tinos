package br.com.iresult.gmfmobile.model.converters

import androidx.room.TypeConverter
import br.com.iresult.gmfmobile.model.bean.Imei
import br.com.iresult.gmfmobile.model.bean.VisitaObservacao
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.util.*

/**
 * Created by victorfernandes on 20/02/19.
 */
class VisitaConverter {

    var gson = Gson()

    @TypeConverter
    fun stringToVisitaList(data: String?): List<VisitaObservacao> {
        if (data == null) {
            return Collections.emptyList()
        }

        val listType = object : TypeToken<List<VisitaObservacao>>() {

        }.getType()

        return gson.fromJson(data, listType)
    }

    @TypeConverter
    fun visitaListToString(visitas: List<VisitaObservacao>): String {
        return gson.toJson(visitas)
    }

    @TypeConverter
    fun fromString(value: String): VisitaObservacao {
        val listType = object : TypeToken<VisitaObservacao>() {

        }.type
        return Gson().fromJson(value, listType)
    }

    @TypeConverter
    fun visitaToString(visita: VisitaObservacao): String {
        return Gson().toJson(visita)
    }

}
