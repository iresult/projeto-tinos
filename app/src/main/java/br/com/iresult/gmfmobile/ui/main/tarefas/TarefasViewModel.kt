package br.com.iresult.gmfmobile.ui.main.tarefas

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import br.com.iresult.gmfmobile.model.bean.Ligacao
import br.com.iresult.gmfmobile.model.bean.Roteiro
import br.com.iresult.gmfmobile.repository.RoteiroRepository
import br.com.iresult.gmfmobile.ui.base.BaseViewModel
import br.com.iresult.gmfmobile.utils.PreferencesKey
import br.com.iresult.gmfmobile.utils.PreferencesManager
import br.com.iresult.gmfmobile.utils.TarefaUtils
import br.com.iresult.gmfmobile.utils.disposedBy


class TarefasViewModel(roteiroRepository: RoteiroRepository, private val preferencesManager: PreferencesManager) : BaseViewModel<TarefasViewModel.State>() {

    private val mRoteiros = MutableLiveData<List<Roteiro>>()
    val roteiros: LiveData<List<Roteiro>> = mRoteiros

    val mTotals = MutableLiveData<HashMap<String, Int>>()
    val totals: LiveData<HashMap<String, Int>> = mTotals

    val mTotalEndereco = MutableLiveData<Int>()
    val mEnderecosConcluidos = MutableLiveData<Int>()
    val mTotalCasas = MutableLiveData<Int>()
    val mCasasConcluidas = MutableLiveData<Int>()
    var mTodasLigacoes = MutableLiveData<List<Ligacao>>()

    var mProgress = MutableLiveData<Int>()
    val progress: LiveData<Int> = mProgress

    enum class State : BaseState

    init {
        mTodasLigacoes.value = ArrayList()
        roteiroRepository.allImoveis().subscribe({ list ->
            mTodasLigacoes.value = list
            Log.d("TarefasViewModel", "Todas as ${mTodasLigacoes.value?.size} ligações carregadas da lista")
            Log.d("TarefasViewModel", "Listando as ${mTodasLigacoes.value?.size} ligações")
        }, {
            Log.e("TarefasViewModel", it.toString())
        }).disposedBy(disposeBag)

        roteiroRepository.getRoteiros().subscribe({
            mRoteiros.value = it
            val roteiros = mRoteiros.value
            roteiros?.forEach { roteiro ->
                val ligacoes = ArrayList<Ligacao>()
                roteiro.tarefas?.addresses?.forEach { rua ->
                    if (rua.ligacoes != null) {
                        ligacoes.addAll(rua.ligacoes)
                    }
                }
                val todasLigacoes = mTodasLigacoes.value
                val totals = TarefaUtils.calculateTotalsByRoteiroLigacoes(roteiro, todasLigacoes as ArrayList<Ligacao>)
                mEnderecosConcluidos.value = totals.get("RUAS_CONCLUIDAS")
                mTotalEndereco.value = totals.get("RUAS_TOTAIS")
                mCasasConcluidas.value = totals.get("CASAS_CONCLUIDAS")
                mTotalCasas.value = totals.get("CASAS_TOTAIS")
                mProgress.value = totals.get("CASAS_CONCLUIDAS_PERCENT")
                mTotals.value = totals
            }
        }, {
            Log.e("TarefasViewModel", it.toString())
        }).disposedBy(disposeBag)
    }

    fun notShowInformatives(): Boolean {
        return preferencesManager.get(PreferencesKey.NOT_SHOW_INFORMATIVES, false) == true
    }
}