package br.com.iresult.gmfmobile.ui.main.tarefas.itinerary.reading

import android.util.Base64
import android.util.Log
import androidx.lifecycle.MutableLiveData
import br.com.iresult.gmfmobile.BuildConfig
import br.com.iresult.gmfmobile.model.bean.*
import br.com.iresult.gmfmobile.model.dao.LeituraGMFDao
import br.com.iresult.gmfmobile.repository.LeituraRepository
import br.com.iresult.gmfmobile.service.LoginService
import br.com.iresult.gmfmobile.utils.Utils
import br.com.iresult.gmfmobile.utils.defaultScheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.apache.commons.lang3.StringUtils
import org.apache.commons.lang3.exception.ExceptionUtils
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class SyncLeituraGMF() {
    var qtdServicos = 0
    var leituraRepository: LeituraRepository? = null
    var loginService: LoginService? = null
    var leituraGMFDao: LeituraGMFDao? = null
    var fileName = ""

    fun enviarLeituraGMF(leitura: Leitura, ligacao: Ligacao, erroMessage: MutableLiveData<String>, mUpdatedUnidade: MutableLiveData<Ligacao>) {

        fileName = ligacao.file!!

        var servicos = leituraRepository?.dataBase?.servicoDao()?.getServicos(leitura.numeroLigacao)?.blockingFirst();

        var usuario = leituraRepository?.dataBase?.usuarioDao()?.getUsuario()?.blockingGet()?.get(0)

        this.qtdServicos = servicos?.size!!

        val sb = montarVisita(fileName, leitura, usuario!!)

        var servicosArr = montarServicos(servicos!!, leitura, sb)

        try {
            doAsync {
                loginService?.syncLeitura(fileName, leitura.sqLeitura.toInt(), servicosArr.size, servicosArr)
                        ?.observeOn(AndroidSchedulers.mainThread())
                        ?.subscribeOn(Schedulers.io())?.subscribe({ response ->
                            leitura.statusTransmissao = 1
                            leituraRepository?.dataBase?.leituraDao()?.updateLeitura(leitura)
                            Log.e("Success", "Leitura enviada com sucesso")
                            uiThread {
                                mUpdatedUnidade.postValue(ligacao)
                            }
                        }, {
                            uiThread {

                                val leituraGMF = LeituraGMF(null, fileName, leitura.sqLeitura.toInt(), servicosArr.size, servicosArr)
                                leituraGMFDao?.insert(leituraGMF)

                                erroMessage.postValue("Não foi possível o envio para GMF neste momento. O envio será realizado de forma automatica assim que possível.")

                            }
                            Log.e("Error", ExceptionUtils.getStackTrace(it))
                        })
            }
        } catch (e: Exception) {
            Log.e("Error", ExceptionUtils.getStackTrace(e))
        }
    }

    fun enviarFotosGMF(numeroLigacao: String, sqLeitura: String, nomeArquivo: String) {
        try {
            doAsync {
                leituraRepository?.dataBase?.fotoGMFDao()
                        ?.getFotos(nomeArquivo, numeroLigacao)
                        ?.defaultScheduler()
                        ?.blockingGet()
                        ?.let { fotos ->

                            if (fotos.isNotEmpty()) {

                                val dsLinhas = fotos.map { it.dsLinha }
                                val blobs = fotos.map { Base64.encodeToString(Utils.getBlobFromPath(it.path), Base64.DEFAULT) }
                                leituraRepository?.sendFotos(nomeArquivo, sqLeitura, dsLinhas, blobs)
                                        ?.observeOn(AndroidSchedulers.mainThread())
                                        ?.subscribeOn(Schedulers.io())?.subscribe({ response ->
                                            uiThread {
                                                Log.e("Success", "Fotos enviadas com sucesso")
                                                fotos.forEach { foto ->
                                                    if (foto.resend) {
                                                        foto.resend = false
                                                        leituraRepository?.dataBase?.fotoGMFDao()?.updateFoto(foto)
                                                    }
                                                }
                                            }
                                        }, {
                                            uiThread {
                                                fotos.forEach { foto ->
                                                    foto.resend = true
                                                    leituraRepository?.dataBase?.fotoGMFDao()?.updateFoto(foto)
                                                }
                                            }
                                            Log.e("sendFotos Error", ExceptionUtils.getStackTrace(it))
                                        })
                            }
                        }
            }
        } catch (e: Exception) {
            Log.e("Error", ExceptionUtils.getStackTrace(e))
        }
    }

    fun enviarFotosOcorrGMF(numeroLigacao: String, sqLeitura: String, nomeArquivo: String) {
        try {
            doAsync {
                leituraRepository?.dataBase?.fotoOcorrGMFDao()
                        ?.getFotosOcorr(nomeArquivo, numeroLigacao)
                        ?.defaultScheduler()
                        ?.blockingGet()
                        ?.let { fotos ->

                            if (fotos.isNotEmpty()) {

                                val dsLinhas = fotos.map { it.dsLinha }
                                val blobs = fotos.map { Base64.encodeToString(Utils.getBlobFromPath(it.path), Base64.DEFAULT) }
                                leituraRepository?.sendFotos(nomeArquivo, sqLeitura, dsLinhas, blobs)
                                        ?.observeOn(AndroidSchedulers.mainThread())
                                        ?.subscribeOn(Schedulers.io())?.subscribe({ response ->
                                            uiThread {
                                                Log.e("Success", "Fotos Ocorrencia enviadas com sucesso")
                                                fotos.forEach { foto ->
                                                    if (foto.resend) {
                                                        foto.resend = false
                                                        leituraRepository?.dataBase?.fotoOcorrGMFDao()?.updateFoto(foto)
                                                    }
                                                }
                                            }
                                        }, {
                                            uiThread {
                                                fotos.forEach { foto ->
                                                    foto.resend = true
                                                    leituraRepository?.dataBase?.fotoOcorrGMFDao()?.updateFoto(foto)
                                                }
                                            }
                                            Log.e("sendFotosOcorr Error", ExceptionUtils.getStackTrace(it))
                                        })
                            }
                        }
            }
        } catch (e: Exception) {
            Log.e("sendFotosOcorr Error", ExceptionUtils.getStackTrace(e))
        }
    }

    private fun montarServicos(servicos: List<Servico>, leitura: Leitura, linha: String): List<String> {
        var servs = ArrayList<String>()

        servs.add(linha)
        var qtd = 1
        for (servico in servicos) {

            val sbs = StringBuffer()

            sbs.append("SALTOWEB.PRC_COLETOR_SIMU_CEL@SALTOWEBH")
            val fim = if (qtd == servicos.size) "FIM" else ""

            sbs.append("(\'\'\'\',");            // inicio
            sbs.append("\'\'" + fim + "\'\',");    // fim

            //Nome do arquivo de retorno
            sbs.append("\'\'");
            sbs.append(fileName);
            sbs.append("\'\',");

            //Sigla da tabela de retorno para o sistema Inteligest.
            sbs.append("\'\'LS01SERV0100001\'\',\'\'")

            //Inicio da linha de dados de retorno do coletor.
            sbs.append("11");
            sbs.append(servico.numReg);
            sbs.append("#");
            sbs.append(servico.matricula); //numeroLigacao ao gravar na tabela de serviços.
            sbs.append("#");
            sbs.append(servico.numeroAviso);
            sbs.append("#");
            sbs.append(servico.anoEmissao);
            sbs.append("#");
            sbs.append(servico.numeroEmissao);
            sbs.append("#");
            sbs.append(servico.codigoTributo);
            sbs.append("#");
            sbs.append(servico.numeroServico);
            sbs.append("#");
            sbs.append(servico.itemServico);
            sbs.append("#");
            sbs.append(servico.codigoRubrica);
            sbs.append("#");
            sbs.append(servico.valorParcela);
            sbs.append("#");
            sbs.append(servico.valorImposto);
            sbs.append("#");
            sbs.append(servico.valorBaseImposto);
            sbs.append("#");
            if (StringUtils.isNotBlank(servico.referenciaParcela)) sbs.append(servico.referenciaParcela) else sbs.append("")
            sbs.append("#");
            if (StringUtils.isNotBlank(servico.statusDesconto)) sbs.append(servico.statusDesconto) else sbs.append("")
            sbs.append("#");
            sbs.append(servico.saldoServico);
            sbs.append("#");
            if (StringUtils.isNotBlank(servico.statusLancamento)) sbs.append(servico.statusLancamento) else sbs.append("")
            sbs.append("\'\')");

            servs.add(sbs.toString())
            qtd++
        }



        return servs
    }

    private fun montarVisita(fileName: String, leitura: Leitura, usuario: Usuario): String {

        val sb = StringBuffer()

        val fim = if (this.qtdServicos === 0) "FIM" else ""

        sb.append("SALTOWEB.PRC_COLETOR_SIMU_CEL@SALTOWEBH")

        sb.append("(\'\'INICIO\'\',")

        sb.append("\'\'$fim\'\',")

        //Nome do arquivo.
        sb.append("\'\'")
        sb.append(fileName)
        sb.append("\'\',")

        //Sigla da tabela de retorno para o sistema Inteligest.
        sb.append("\'\'LS01LIGA0200001\'\',\'\'")

        //Inicio dos dados da linha
        sb.append("11")
        sb.append(leitura.numReg)
        sb.append("#")
        sb.append(leitura.numeroLigacao)
        sb.append("#")
        sb.append(leitura.codigoGrupo)
        sb.append("#")
        sb.append(leitura.mesLancamento)
        sb.append("#")
        sb.append(leitura.anoLancamento)
        sb.append("#")
        sb.append(usuario.codigo)
        sb.append("#")
        sb.append(leitura.leituraAtual)
        sb.append("#")
        sb.append(leitura.codLeitura)
        sb.append("#")
        sb.append(leitura.codLeituraInterno)
        sb.append("#")
//		sb.append(leitura.dataLeitura.toString(Settings.DATE_YMD));
        if (StringUtils.isNotBlank(leitura.dataLeitura)) sb.append(leitura.dataLeitura) else sb.append("")
        sb.append("#")
        if (StringUtils.isNotBlank(leitura.horaLeitura)) sb.append(leitura.horaLeitura) else sb.append("")
        sb.append("#")
        sb.append(leitura.quantidadeTentativas)
        sb.append("#")
        sb.append(leitura.consumoMedido)
        sb.append("#")
        sb.append(leitura.consumoMedidoEE)
        sb.append("#")
        sb.append(leitura.conusumoFaturado)
        sb.append("#")
        sb.append(leitura.conusumoFaturadoEE)
        sb.append("#")
        sb.append(leitura.valorTotal)
        sb.append("#")
        sb.append(leitura.qtdCons)
        sb.append("#")
        sb.append(leitura.criterioFaturamento)
        sb.append("#")
        if (StringUtils.isNotBlank(leitura.descStatus)) sb.append(leitura.descStatus) else sb.append("")
        sb.append("#")
        if (StringUtils.isNotBlank(leitura.descDica)) sb.append(leitura.descDica) else sb.append("")
        sb.append("#")
        sb.append(if (StringUtils.isNotBlank(leitura.descObs)) leitura.descObs + Utils.space(60 - leitura.descObs!!.length) else Utils.space(60))
        sb.append("#")
        sb.append(leitura.credConsumo)
        sb.append("#")
        //if (StringUtils.isNotBlank(leitura.descObs)) sb.append(leitura.descObs?.trim() + Utils.space(35 - leitura.descObs?.length!!)) else sb.append(Utils.space(35))
        //if (StringUtils.isNotBlank(leitura.descMotivoEntrega)) sb.append(leitura.descMotivoEntrega + Utils.space(25 - leitura.descMotivoEntrega?.length!!)) else sb.append(Utils.space(25))
        sb.append(leitura.descMotivoEntrega)
        sb.append("#")
        sb.append("#")
        sb.append(leitura.qtdEmissoes)
        sb.append("#")
        if (StringUtils.isNotBlank(leitura.codEntregaNotificacao)) sb.append(leitura.codEntregaNotificacao?.toInt()) else sb.append("")
        sb.append("#")
        sb.append(100)
        sb.append("#")
        sb.append(leitura.sqLeitura)
        sb.append("#")
        sb.append(StringUtils.rightPad(BuildConfig.VERSION_NAME, 10, ' '))
        sb.append("#")
        sb.append(leitura.valorMinimo)
        sb.append("#")
        sb.append(leitura.statusRegistro)

        sb.append("\'\')")

        return sb.toString()

    }
}
