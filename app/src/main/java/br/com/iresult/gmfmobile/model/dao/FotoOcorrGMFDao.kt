package br.com.iresult.gmfmobile.model.dao

import androidx.room.*
import br.com.iresult.gmfmobile.model.bean.FotoGMF
import br.com.iresult.gmfmobile.model.bean.FotoOcorrGMF
import io.reactivex.Observable
import io.reactivex.Single

@Dao
interface FotoOcorrGMFDao {

    @Query("SELECT * FROM fotoOcorrGMF WHERE nomeArquivo = :nomeArquivo AND numLigacao = :numLigacao order by uuid ASC")
    fun getFotosOcorr(nomeArquivo: String, numLigacao: String): Single<List<FotoOcorrGMF>>

    @Query("SELECT * FROM fotoOcorrGMF WHERE resend = 1")
    fun getFotosOcorrToResend(): Single<List<FotoOcorrGMF>>

    @Query("SELECT * FROM fotoOcorrGMF WHERE path = :path LIMIT 1")
    fun getFotoOcorrByPath(path: String): Single<FotoOcorrGMF>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(fotoOcorrGMF: FotoOcorrGMF)

    @Delete
    fun delete(fotoOcorrGMF: FotoOcorrGMF)

    @Update
    fun updateFoto(fotoOcorrGMF: FotoOcorrGMF)
}
