package br.com.iresult.gmfmobile.service

import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface FotoService {

    @FormUrlEncoded
    @POST("save_data_foto.php")
    fun sendFotos(@Field("NOM_ARQUIVO") nomeArquivo: String,
                  @Field("SEQ_LEITURA") sqLeitura: String,
                  @Field("DS_LINHA[]") dsLinha: List<String>,
                  @Field("BLB_FOTO[]") blob: List<String>): Observable<ResponseBody>
}