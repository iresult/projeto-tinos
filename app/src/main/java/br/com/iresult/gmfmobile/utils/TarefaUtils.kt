package br.com.iresult.gmfmobile.utils

import android.util.Log
import br.com.iresult.gmfmobile.model.bean.Imei
import br.com.iresult.gmfmobile.model.bean.Justificativa
import br.com.iresult.gmfmobile.model.bean.Ligacao
import br.com.iresult.gmfmobile.model.bean.Roteiro
import br.com.iresult.gmfmobile.ui.widget.HomeStatus
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class TarefaUtils {
    companion object {
        fun calculateTotalsByRoteiroLigacoes(r: Roteiro, listaLigacoes: List<Ligacao>): HashMap<String, Int> {
            val resultMap: HashMap<String, Int> = HashMap()
            Log.d("TarefasViewModel", "calculateTotalsByRoteiro()   Nome: ${r.nome}")
            val addresses = r.tarefas?.addresses
            Log.d("TarefasViewModel", "Enderecos na Tarefa: ${addresses?.size}")
            var tempCasasConcluidas = 0
            val tempTotalRuas = addresses?.size
            var tempTotalCasas = 0
            var tempTotalRuasConcluidas = 0
            val tempTotalNotificacoes = r.notificacoes?.size
            var tempNotificacoesEntregues = 0
            var leiturasPendentes = 0
            var leiturasEnviadas = 0
            r.leitura?.forEach {

                 if (it.codEntregaNotificacao != null
                         && it.codEntregaNotificacao != "") {
                    tempNotificacoesEntregues += 1
                 }


                 //        && it.codEntregaNotificacao!="00"
             //   if (it.codLeitura != 0) {
             //       tempNotificacoesEntregues += 1
             //   }

                if (it.statusTransmissao == 0) {
                    leiturasPendentes += 1
                } else {
                    leiturasEnviadas += 1
                }

            }

            if (addresses != null) {
                for (addr in addresses) {
                    Log.d("TarefasViewModel", "Analisando endereco ${addr.nome}")
                    var enderecoConcluido = true
                    val tempLigacoes = getLigacoesFromAddress(addr.nome, listaLigacoes)
                    Log.d("TarefasViewModel", "Analisando ligacoes neste endereco ${tempLigacoes.size}")
                    tempTotalCasas += tempLigacoes.size
                    for (ligacao in tempLigacoes) {
                        Log.d("TarefasViewModel", "Ligacao: ${ligacao.numero}   Status: ${ligacao.status}   Concluido: ${ligacao.concluido}")
                        if (ligacao.status != null
                                && ligacao.status != HomeStatus.AGORA.name
                                && ligacao.status != HomeStatus.PROXIMO.name) {
                            tempCasasConcluidas += 1
                        } else {
                            enderecoConcluido = false
                        }
                    }
                    if (enderecoConcluido) {
                        tempTotalRuasConcluidas += 1
                    }
                }
            }
            val percentage = (tempCasasConcluidas.toFloat() / tempTotalCasas.toFloat())

            resultMap["RUAS_CONCLUIDAS"] = tempTotalRuasConcluidas
            resultMap["RUAS_TOTAIS"] = tempTotalRuas!!.toInt()
            resultMap["CASAS_CONCLUIDAS"] = tempCasasConcluidas
            resultMap["CASAS_TOTAIS"] = tempTotalCasas
            resultMap["NOTIFICACOES_TOTAIS"] = tempTotalNotificacoes!!.toInt()
            resultMap["NOTIFICACOES_NAO_ENTREGUES"] = (tempTotalNotificacoes!!.toInt() - tempNotificacoesEntregues)
            resultMap["CASAS_CONCLUIDAS_PERCENT"] = (percentage * 100).toInt()
            resultMap["LEITURAS_PENDENTES"] = leiturasPendentes
            resultMap["LEITURAS_ENVIADAS"] = leiturasEnviadas
            return resultMap
        }

        private fun getLigacoesFromAddress(nome: String, listaLigacoes: List<Ligacao>): List<Ligacao> {
            val tempLigacoes: ArrayList<Ligacao> = ArrayList()
            // var listaLigacoes = mTodasLigacoes.value
            Log.d("TarefasViewModel", "Procurando ligacoes de $nome em ${listaLigacoes.size}")

            for (lig in listaLigacoes) {
                Log.d("TarefasViewModel", "Ligacao endereco ${lig.enderecoLigacao}")
                if (lig.rua == nome) {
                    tempLigacoes.add(lig)
                }
            }

            return tempLigacoes
        }

        fun findImeiInImeiList(imei : String, imeiList : List<Imei>) : Int {
            var index : Int = -1
            imeiList.forEach { imeiItem ->
                index++
                if (imei == imeiItem.codigoImei) {
                    return index
                }
            }
            return -1
        }

        fun roteiroApprovedForToday(roteiro : Roteiro) : Boolean {
            val parametros = roteiro.parametros
            parametros.forEach { parametroItem ->
                val dataDataIni = Utils.getDateTimeFromString(parametroItem.dataIni.toString() + "000000")
                val dataDataFim = Utils.getDateTimeFromString(parametroItem.dataFim.toString() + "235959")
                val currentTime = Date()
                if (dataDataIni != null && dataDataFim != null) {
                    if (currentTime.time >= dataDataIni.time && currentTime.time <= dataDataFim.time) {
                        return true
                    }
                }
            }
            // Alterar para return false, para ativar a filtragem de tarefas autorizadas para o dia
            return false
        }

//        fun <A, B> zipLiveData(a: LiveData<A>, b: LiveData<B>): LiveData<Pair<A, B>> {
//            return MediatorLiveData<Pair<A, B>>().apply {
//                var lastA: A? = null
//                var lastB: B? = null
//
//                fun update() {
//                    val localLastA = lastA
//                    val localLastB = lastB
//                    if (localLastA != null && localLastB != null)
//                        this.value = Pair(localLastA, localLastB)
//                }
//
//                addSource(a) {
//                    lastA = it
//                    update()
//                }
//                addSource(b) {
//                    lastB = it
//                    update()
//                }
//            }
//        }

        fun getJustificativaByOcorrenciaECodigo(lista: List<Justificativa>, ocorrencia : Int,  cod: String) : Justificativa {
            var defaultJust = Justificativa("000001", 0, "")
            lista.forEach {
                if (it.codigo == ocorrencia) {
                    defaultJust = it
                    if (it.numReg == cod) {
                        return it
                    }
                }
            }
            return defaultJust
        }
    }
}
