package br.com.iresult.gmfmobile.repository

import br.com.iresult.gmfmobile.model.dao.*


class VisitaObservacaoRepository(private val visitaObservacaoDao: VisitaObservacaoDao) {

    fun gravarObservacao(numeroRegistro: Int, descricao : String) : Int {
        return visitaObservacaoDao.gravarObservacao(numeroRegistro, descricao)
    }
}
