package br.com.iresult.gmfmobile.model.dao

import androidx.room.*
import br.com.iresult.gmfmobile.model.bean.Imei
import io.reactivex.Observable

@Dao
interface ImeiDao {

    @Query("SELECT distinct * FROM Imei ")
    fun getImei(): Observable<Imei>

    @Query("SELECT * FROM Imei")
    fun getImeis(): Observable<List<Imei>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(imeis: List<Imei>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(imei: Imei)

    @Update
    fun updateImei(imei: Imei)

}
