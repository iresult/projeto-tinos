package br.com.iresult.gmfmobile.ui.main.tarefas.itinerary.reading

import android.annotation.SuppressLint
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import br.com.iresult.gmfmobile.model.Visita
import br.com.iresult.gmfmobile.model.bean.*
import br.com.iresult.gmfmobile.model.database.AppDataBase
import br.com.iresult.gmfmobile.print.ZebraConnection
import br.com.iresult.gmfmobile.repository.LeituraRepository
import br.com.iresult.gmfmobile.service.LoginService
import br.com.iresult.gmfmobile.ui.base.BaseViewModel
import br.com.iresult.gmfmobile.ui.base.FormaEntregaDialog
import br.com.iresult.gmfmobile.ui.widget.HomeStatus
import br.com.iresult.gmfmobile.utils.TarefaUtils
import br.com.iresult.gmfmobile.utils.Utils
import br.com.iresult.gmfmobile.utils.defaultScheduler
import br.com.iresult.gmfmobile.utils.disposedBy
import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import org.apache.commons.lang3.StringUtils
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


/**
 * Created by victorfernandes on 24/02/19.
 */
class LeituraViewModel(val leituraRepository: LeituraRepository, val dataBase: AppDataBase, val loginService: LoginService) : BaseViewModel<LeituraViewModel.State>() {

    enum class State : BaseState {
        OCORRENCIA_IMPEDITIVA,
        IMPRESSAO,
        ENTREGA
    }

    private val LIGACAO_OBS_TEXT_SIZE = 60

    private val habilitarBotaoEnvio = MutableLiveData<Boolean>()
    private val habilitarBotaoOcorrencia = MutableLiveData<Boolean>()

    val mUnidade = MutableLiveData<Ligacao>()
    val unidade: LiveData<Ligacao> = mUnidade

    private val mRua = MutableLiveData<Address>()
    val address: LiveData<Address> = mRua

    private val mOcorrencia = MutableLiveData<OcorrenciaLeitura>()
    val ocorrencia: LiveData<OcorrenciaLeitura> = mOcorrencia

    var segundaVia = false
    var quantidadeEmissoes = 0

    //TODO Leitura
    private val mLeitura = MutableLiveData<String>()
    val leitura: LiveData<String> = mLeitura

    //TODO Ocorrencia
    private val mTiposOcorrencia = MutableLiveData<List<Ocorrencia>>()
    val tiposOcorrencia: LiveData<List<Ocorrencia>> = mTiposOcorrencia

    private val mTipoOcorrenciaSelecionada = MutableLiveData<Ocorrencia?>()
    val tipoOcorrenciaSelecionada: LiveData<Ocorrencia?> = mTipoOcorrenciaSelecionada

    private val mJustificativaSelecionada = MutableLiveData<Justificativa?>()
    val justificativaSelecionada: LiveData<Justificativa?> = mJustificativaSelecionada

    private val mOcorrenciaDescricao = MutableLiveData<String>()
    val ocorrenciaDescricao: LiveData<String> = mOcorrenciaDescricao

    private val mImagensOcorrencia = MutableLiveData<MutableList<FotoOcorrGMF>>()
    val imagensOcorrencia: LiveData<MutableList<FotoOcorrGMF>> = mImagensOcorrencia

    private val justificativas = MutableLiveData<List<Justificativa>>()

    private val mJustificativasOcorrencia = MutableLiveData<List<Justificativa>>()
    val justificativasOcorrencia: LiveData<List<Justificativa>> = mJustificativasOcorrencia

    private val mEnableDisableJustify = MutableLiveData<Boolean>()
    val enableDisableJustify: LiveData<Boolean> = mEnableDisableJustify

    private val mHabilitarCampoTexto = MutableLiveData<Boolean>()
    val isHabilitarCampoTexto: LiveData<Boolean> = mHabilitarCampoTexto

    private val mObservacaoDescricao = MutableLiveData<String>()
    val observacaoDescricao: LiveData<String> = mObservacaoDescricao

    private val mLeituraDescricao = MutableLiveData<String>()
    val leituraDescricao: LiveData<String> = mLeituraDescricao

    //TODO ORDEM SERVICO
    private val mServicos = MutableLiveData<List<Rubrica>>()
    val servicos: LiveData<List<Rubrica>> = mServicos

    private val mServicoSelecionado = MutableLiveData<Rubrica>()
    val servicoSelecionado: LiveData<Rubrica> = mServicoSelecionado

    private val mTextoServico = MutableLiveData<String>()
    val textoServico: LiveData<String> = mTextoServico

    private val mImagensServico = MutableLiveData<MutableList<File>>()
    val imagensServico: LiveData<MutableList<File>> = mImagensServico

    val mConsumoZeroDialog = MutableLiveData<String>()
    val consumoZeroDialog: LiveData<String> = mConsumoZeroDialog

    private val mServicosLeituras = MutableLiveData<List<ServicoLeitura>>()
    val servicosLeitura: LiveData<List<ServicoLeitura>> = mServicosLeituras

    val mErroDialogCloseMessage = MutableLiveData<String>()
    val erroDialogCloseMessage: LiveData<String> = mErroDialogCloseMessage

    val mCorteDialogMessage = MutableLiveData<String>()
    val corteDialogMessage: LiveData<String> = mCorteDialogMessage

    val mErrorMessage = MutableLiveData<String>()
    val errorMessage: LiveData<String> = mErrorMessage

    private val mErroLeitura = MutableLiveData<String>()
    val erroLeitura: LiveData<String> = mErroLeitura

    val mEnviarOcorrenciaGMF = MutableLiveData<String>()
    val enviarOcorrenciaGMF: LiveData<String> = mEnviarOcorrenciaGMF

    val mProcessDialog = MutableLiveData<String>()
    val processDialog: LiveData<String> = mProcessDialog

    val mConfirmeLeituraMessage = MutableLiveData<String>()
    val confirmeLeituraMessage: LiveData<String> = mConfirmeLeituraMessage

    val mOcorrenciaNotificacaoMessage = MutableLiveData<String>()
    val ocorrenciaNotificacaoMessage: LiveData<String> = mOcorrenciaNotificacaoMessage

    val mNotificacaoLeitura = MutableLiveData<String>()
    val notificacaoLeitura: LiveData<String> = mNotificacaoLeitura

    private val mUpdateSegundaVia = MutableLiveData<Boolean>()
    val updateSegundaVia: LiveData<Boolean> = mUpdateSegundaVia


    val mUpdatedUnidade = MutableLiveData<Ligacao>()
    val updatedUnidade: LiveData<Ligacao> = mUpdatedUnidade

    private val mLeituraObject = MutableLiveData<Leitura>()
    val leituraObject: LiveData<Leitura> = mLeituraObject


    private val mUsuario = MutableLiveData<Usuario>()
    val usuario: LiveData<Usuario> = mUsuario


    private val mProgressBarShown = MutableLiveData<Boolean>()
    val progressBarShown: LiveData<Boolean> = mProgressBarShown

    private val mImagensLeitura = MutableLiveData<MutableList<FotoGMF>>()
    val imagensLeitura: LiveData<MutableList<FotoGMF>> = mImagensLeitura

    private val mNomeArquivo = MutableLiveData<String>()
    val nomeArquivo: LiveData<String> = mNomeArquivo

    init {
        habilitarBotaoEnvio.value = true
        habilitarBotaoOcorrencia.value = true
        mLeitura.value = ""

        dataBase.usuarioDao()
                .getUsuario()
                .defaultScheduler()
                .subscribe({
                    mUsuario.value = it.firstOrNull()
                }, {
                    Log.e("MainViewModel", it.toString())
                }).disposedBy(disposeBag)

    }

    fun initModel(address: Address, ligacao: Ligacao, roteiro: String) {
        this.mRua.value = address
        this.mUnidade.value = ligacao
        ligacao.file = roteiro
        val roteiroObserver: Observable<Roteiro> =
                leituraRepository.dataBase
                        .roteiroDao()
                        .getRoteiro(roteiro)
                        .defaultScheduler()

        val ocorrenciaObserver: Observable<OcorrenciaLeitura> = leituraRepository.dataBase
                .ocorrenciaLeituraDao()
                .getOcorrenciaLeitura(ligacao.numeroLigacao)
                .defaultScheduler()

        roteiroObserver
                .subscribe({
                    mNomeArquivo.value = it.nome
                    mTiposOcorrencia.value = it.ocorrencias
                    Log.i("LeituraViewModel", "Tipos de Ocorrencias")
                    it.ocorrencias?.forEach { ocorr ->
                        Log.i("LeituraViewModel",
                                "Ocorrencia: ${ocorr.codigo}  Texto: ${ocorr.descricao}  Status Aeita: ${ocorr.statusAceita}")

                    }
                    justificativas.value = it.justificativas
                    mServicos.value = it.rubricas

                    leituraRepository.dataBase
                            .fotoOcorrGMFDao()
                            .getFotosOcorr(nomeArquivo.value
                                    ?: "", ligacao.numeroLigacao.toString())
                            .defaultScheduler()
                            .subscribe({ fotos ->
                                mImagensOcorrencia.value = fotos.toMutableList()
                            }, {

                            })
                            .disposedBy(disposeBag)

                }, {
                    Log.e("LeituraViewModel", it.toString())
                })
                .disposedBy(disposeBag)

        leituraRepository.dataBase
                .servicoDao()
                .getServicosLeitura(ligacao.numeroLigacao)
                .defaultScheduler()
                .subscribe({
                    mServicosLeituras.value = it
                }, {
                    Log.e("LeituraViewModel", it.toString())
                }).disposedBy(disposeBag)

        Observable
                .zip(roteiroObserver,
                        ocorrenciaObserver,
                        BiFunction<Roteiro, OcorrenciaLeitura, Pair<Roteiro, OcorrenciaLeitura>> { rot, ocorr ->
                            Pair(rot, ocorr)
                        })
                .subscribe {
                    val rot = it.first
                    val ocorr = it.second
                    mOcorrencia.value = ocorr
                    mTipoOcorrenciaSelecionada.value = it.second.ocorrencia
                    val listaJustificativas = rot.justificativas ?: ArrayList()
                    justificativas.value = listaJustificativas
                    carregarJustificativasConformeOcorrencia(mOcorrencia.value?.ocorrencia!!)
                    definePermissaoParaDigitarLeitura(mOcorrencia.value?.ocorrencia!!)
                    Log.i("LeituraViewModel", "Lendo Ocorrencia:")
                    Log.i("LeituraViewModel", "Numero da Ligação: ${ocorr.numeroLigacao}  " +
                            " TipoOcorrencia: ${ocorr.ocorrencia.codigo}   Justificativa Cod: ${ocorr?.cod_justificativa}")
                    mJustificativaSelecionada.value = TarefaUtils.getJustificativaByOcorrenciaECodigo(listaJustificativas, ocorr.ocorrencia.codigo, ocorr.cod_justificativa)

                }

        leituraRepository.dataBase
                .leituraDao()
                .getLeitura(ligacao.numeroLigacao)
                .defaultScheduler()
                .subscribe({
                    if (it.isNotEmpty()) {
                        mLeituraObject.value = it[0]
                        mLeituraDescricao.value = mLeituraObject?.value?.descObs
                                ?: String(CharArray(LIGACAO_OBS_TEXT_SIZE)).replace('\u0000', ' ')
                        mOcorrenciaDescricao.value = getOcorrenciaTextoFromLeitura().trimEnd()
                        mObservacaoDescricao.value = getObservacaoTextoFromLeitura().trimEnd()
                        if (it[0].qtdEmissoes >= 1) {
                            this.quantidadeEmissoes = it[0].qtdEmissoes
                            this.segundaVia = true
                            mUpdateSegundaVia.value = true
                        }
                    }
                }, {
                    Log.e("LeituraViewModel", it.toString())
                }).disposedBy(disposeBag)
    }

    fun formatedOccourenceList(): Array<String> {
        return tiposOcorrencia.value?.map {
            "${it.codigo} - ${it.descricao}"

        }?.toTypedArray() ?: emptyArray()
    }

    fun formatedJustifyList(): Array<String> {
        return justificativasOcorrencia.value?.map {
            it.descricao ?: "--------------"
        }?.toTypedArray() ?: emptyArray()
    }

    fun habilitarBotaoOcorrencia() = habilitarBotaoOcorrencia

    fun setLeitura(valor: String) {

        if (!mLeitura.value.equals(valor)) {
            mLeitura.value = valor
        }
    }

    fun statusRegistroDescription(code: String) = when (code) {
        "1" -> "Ativo"
        "2" -> "Cortada"
        "3" -> "Corte a pedido"
        else -> "Não informado"
    }

    fun deveHabilitarBotaoEnvio() = habilitarBotaoEnvio

    fun isSegundaVia() = updateSegundaVia

    private fun definePermissaoParaDigitarLeitura(ocorr: Ocorrencia) {
        mEnableDisableJustify.value = mTipoOcorrenciaSelecionada.value?.statusAceita == "N"
    }

    private fun carregarJustificativasConformeOcorrencia(ocorr: Ocorrencia) {
        if (mTipoOcorrenciaSelecionada.value?.statusJustificativa == Ocorrencia.STATUS_JUSTIFICADO) {
            val justificativas = justificativas.value?.filter {
                it.codigo == ocorr.codigo
            }
            mJustificativasOcorrencia.value = justificativas
        }
    }

    fun selecionaTipoOcorrencia(posicao: Int) {
        mTipoOcorrenciaSelecionada.value = mTiposOcorrencia.value?.get(posicao)
        val tipoOcorrencia = mTipoOcorrenciaSelecionada.value ?: return
        carregarJustificativasConformeOcorrencia(tipoOcorrencia)
    }

    fun selecionaJustificativa(posicao: Int) {
        mJustificativaSelecionada.value = mJustificativasOcorrencia.value?.get(posicao)
        val justif = mJustificativaSelecionada.value ?: return
        Log.i("LeituraViewModel", "Justificativa Selecionada Pos: $posicao  " +
                "Codigo ${justif.codigo}   Numero Registro ${justif.numReg}   Descricao: ${justif.descricao}")
        mHabilitarCampoTexto.value = justificativaSelecionada.value?.descricao?.contains("texto", true)
    }

    private fun splitTextoInMiddle(texto: String?, siz: Int): Array<String> {
        var currObs = texto ?: String(CharArray(siz)).replace('\u0000', ' ')
        val half = siz / 2
        currObs = currObs.padEnd(siz, ' ')
        val textPart1 = currObs.substring(0, half)
        val textPart2 = currObs.substring(half, siz)
        return arrayOf(textPart1, textPart2)
    }

    private fun getObservacaoTextoFromLeitura(): String {
        val splitted = splitTextoInMiddle(mLeituraObject.value?.descObs, LIGACAO_OBS_TEXT_SIZE)
        val textPartObs = splitted[1]
        return textPartObs
    }

    private fun getOcorrenciaTextoFromLeitura(): String {
        val splitted = splitTextoInMiddle(mLeituraObject.value?.descObs, LIGACAO_OBS_TEXT_SIZE)
        val textPartNot = splitted[0]
        return textPartNot
    }

    private fun getObservacaoTextoCompleto(): String {
        val half = (LIGACAO_OBS_TEXT_SIZE / 2)
        return Utils.truncateText(mOcorrenciaDescricao.value, half) + Utils.truncateText(mObservacaoDescricao.value, half)
    }


    fun salvarOcorrencia() {
        Log.i("LeituraViewModel", "Salvando a ocorrencia")
        mProgressBarShown.value = true
        val numeroLigacao = mUnidade.value?.numeroLigacao ?: return
        val tipoOcorrenciaSelecionada = mTipoOcorrenciaSelecionada.value ?: return
        val mJustificativas = mJustificativasOcorrencia.value ?: ArrayList()
        val justificativaEscolhida = mJustificativaSelecionada.value
                ?: TarefaUtils.getJustificativaByOcorrenciaECodigo(mJustificativas, tipoOcorrenciaSelecionada.codigo, "000001")
        Log.i("LeituraViewModel", "Numero da Ligação: $numeroLigacao  " +
                " TipoOcorrencia: ${tipoOcorrenciaSelecionada.codigo}   Justificativa Cod: ${justificativaEscolhida.codigo}")
        val ocorrencia = OcorrenciaLeitura(numeroLigacao, tipoOcorrenciaSelecionada, justificativaEscolhida.numReg)
        mOcorrencia.value = ocorrencia


        doAsync {
            leituraRepository.dataBase.ocorrenciaLeituraDao().insertOcorrencia(ocorrencia)
            Log.i("LeituraViewModel", "Ocorrencia Salva")

            uiThread {
                //it.mOcorrencia.value = ocorrencia
                it.habilitarBotaoOcorrencia.value = false
                it.mHabilitarCampoTexto.value = !tipoOcorrenciaSelecionada.isOcorrenciaImpeditiva()
            }
            val leitura = leituraRepository.dataBase.leituraDao().getLeitura(numeroLigacao).defaultScheduler().blockingFirst()?.get(0)
            if (leitura != null) {
                leituraRepository.dataBase.leituraDao().gravarObservacao(numeroLigacao, getObservacaoTextoCompleto())
            }
            mProgressBarShown.value = false
        }
    }

    /**
     * Limpa as informacoes de ocorrencia
     */
    fun limparOcorrencia() {
        mTipoOcorrenciaSelecionada.value = null
        mJustificativaSelecionada.value = null
        mOcorrenciaDescricao.value = null
    }

    fun getFormattedAddress(compl: String?): String {
        val address = address.value
        return "${address?.nome}\n" +
                "Compl. $compl\n" +
                "${address?.bairro} ${address?.cep}"
    }

    fun observacaoOcorrenciaValueChanged(value: String) {
        mOcorrenciaDescricao.value = value
    }

    fun addImagemOcorrencia(path: String) {
        if (mImagensOcorrencia.value == null) {
            mImagensOcorrencia.value = ArrayList()
        }

        var usuario = leituraRepository?.dataBase?.usuarioDao()?.getUsuario()?.blockingGet()?.get(0)


        mLeituraObject.value?.let { leitura ->

            mNomeArquivo.value?.let { nomeArquivo ->
                val fotoGMF = FotoOcorrGMF(null, nomeArquivo, leitura.numeroLigacao.toString(), leitura.sqLeitura.toInt(), montaFotoDSLinha(File(path), leitura, usuario!!), path)
                mImagensOcorrencia.value?.add(fotoGMF)
                mImagensOcorrencia.postValue(mImagensOcorrencia.value)
                leituraRepository.dataBase.fotoOcorrGMFDao().insert(fotoGMF)
            }
        }
    }

    fun removeImageOccourence(file: File) {

        leituraRepository.dataBase.fotoOcorrGMFDao().getFotoOcorrByPath(file.absolutePath)
                .defaultScheduler()
                .subscribe({
                    leituraRepository.dataBase.fotoOcorrGMFDao().delete(it)
                    mImagensOcorrencia.value?.remove(it)
                    mImagensOcorrencia.value = mImagensOcorrencia.value
                }, {
                    Log.e("getFotoOcorrByPath", it.toString())
                })
                .disposedBy(disposeBag)
    }

    fun addImagemServico(file: File) {
        if (mImagensServico.value == null) {
            mImagensServico.value = ArrayList()
        }
        mImagensServico.value?.add(file)
        mImagensServico.postValue(mImagensServico.value)

    }

    fun removeImageServiceOrder(file: File) {
        mImagensServico.value?.remove(file)
        mImagensServico.value = mImagensServico.value
    }

    fun selecionarServico(servico: Int) {
        mServicoSelecionado.value = mServicos.value?.get(servico)
    }

    fun onTextoServicoChanged(value: String) {
        if (!mTextoServico.value.equals(value)) {
            mTextoServico.value = value
        }
    }

    fun salvarServico() {
        val numeroLigacao = mUnidade.value?.numeroLigacao ?: return
        val servicoSelecionado = mServicoSelecionado.value ?: return
        val servico = ServicoLeitura(0, numeroLigacao, mTextoServico.value, servicoSelecionado,
                mImagensServico.value
        )

        doAsync { leituraRepository.dataBase.servicoDao().insert(servico) }

        mTextoServico.value = null
        mServicoSelecionado.value = null
        leituraRepository.dataBase
                .servicoDao()
                .getServicosLeitura(numeroLigacao)
                .defaultScheduler()
                .subscribe({
                    mServicosLeituras.value = it
                }, {
                    Log.e("LeituraViewModel", it.toString())
                }).disposedBy(disposeBag)
    }

    fun removerServico(servicoLeitura: ServicoLeitura) {
        doAsync { leituraRepository.dataBase.servicoDao().delete(servicoLeitura) }
        val numeroLigacao = mUnidade.value?.numeroLigacao ?: return
        leituraRepository.dataBase
                .servicoDao()
                .getServicosLeitura(numeroLigacao)
                .defaultScheduler()
                .subscribe({
                    mServicosLeituras.value = it
                }, {
                    Log.e("LeituraViewModel", it.toString())
                }).disposedBy(disposeBag)
    }

    fun atualizarServicos() {
        val numeroLigacao = mUnidade.value?.numeroLigacao ?: return
        leituraRepository.dataBase
                .servicoDao()
                .getServicosLeitura(numeroLigacao)
                .defaultScheduler()
                .subscribe({
                    mServicosLeituras.value = it
                }, {
                    Log.e("LeituraViewModel", it.toString())
                }).disposedBy(disposeBag)
    }

    fun printInvoice(qtdTentativas: Int, visita: Visita) {
        if (quantidadeEmissoes >= 1) {

            var notificacao = visita.ligacao?.numeroLigacao?.let { leituraRepository.dataBase?.notificacaoDao()?.buscarNotificacao(it) }
            if (notificacao != null && !notificacao.isEmpty()) {
                mOcorrenciaNotificacaoMessage.value = "Deseja imprimir a notificacao ?"
                return
            }

            setState(State.IMPRESSAO)
        } else {

            mLeitura.value?.let { leit ->

                when {
                    StringUtils.isBlank(leit) -> {

                        mErroLeitura.value = "LEITURA NÃO INFORMADA. PROSSEGUE?"
                        return

                    }
                    leit.count() > 6 -> {

                        mErrorMessage.value = "ATENÇÃO: REVISE A LEITURA"
                        return

                    }

                    // consumo igual ao último
                    leit.toInt() == visita.ligacao?.leituraAnteriorInt!! -> {

                        mConfirmeLeituraMessage.value = "Consumo Zero (0) \n Confirma ?"
                        return
                    }

                    // consumo igual ao último
                    visita.ligacao?.leituraMaxima!! > 0 &&
                            leit.toInt() < visita.ligacao?.leituraAnteriorInt!! -> {

                        mConfirmeLeituraMessage.postValue("Atenção... Confirme a Leitura")
                        return
                    }

                    // consumo igual ao último
                    visita.ligacao?.leituraMaxima!! > 0 &&
                            leit.toInt() > visita.ligacao?.leituraMaxima!! -> {

                        mErrorMessage.value = "Leitura maior que número de ponteiros, Confirme"
                        return
                    }

                    leit.toInt() < (visita.ligacao?.leituraAnteriorInt!! + visita.ligacao?.consumoMinimoB!!) -> {

                        mErroLeitura.value = "Fora de Faixa, Confirme."
                        return
                    }

                    leit.toInt() > (visita.ligacao?.leituraAnteriorInt!! + visita.ligacao?.consumoMaximoB!!) -> {

                        mErroLeitura.value = "Fora de Faixa, Confirme."
                        return
                    }

                    else -> {


                        var notificacao = visita.ligacao?.numeroLigacao?.let { leituraRepository.dataBase?.notificacaoDao()?.buscarNotificacao(it) }
                        if (notificacao != null && !notificacao.isEmpty()) {
                            mOcorrenciaNotificacaoMessage.value = "Deseja imprimir a notificacao ?"
                            return
                        }

                        setState(State.IMPRESSAO)
                    }
                }
            }
        }
    }

    fun isImpressoraConectada(): Boolean {
        try {
            return (ZebraConnection()).isPortOpen()
        } catch (e: Exception) {
            return false;
        }
        return false
    }

    fun saveUnidade(statusEntrega: FormaEntregaDialog.FormaEntrega, motivo: String) {
        mUnidade.value?.let { ligacao ->

            ligacao.status = when (statusEntrega) {
                FormaEntregaDialog.FormaEntrega.FORMA_ENTREGA_NAO_ENTREGUE -> HomeStatus.NAO_ENTREGUE.name
                else -> HomeStatus.CONCLUIDO.name
            }

            doAsync {
                leituraRepository.dataBase.ligacaoDao().updateLigacao(ligacao).subscribe({ numberOfRows ->

                    val leitura = leituraRepository.dataBase.leituraDao()
                            .getLeitura(ligacao.numeroLigacao).defaultScheduler().blockingFirst()?.get(0)
                    if (leitura != null) {
                        leitura.tipoEntrega = StringUtils.leftPad(statusEntrega.id.toString(), 2, "0")
                        if (statusEntrega.id != 7) {
                            leitura.descMotivoEntrega = leitura.tipoEntrega + " - " + FormaEntregaDialog.FormaEntrega.values()[statusEntrega.id - 1].title.toUpperCase()
                        } else {
                            leitura.descMotivoEntrega = motivo
                        }
                        leituraRepository.dataBase.leituraDao().updateLeitura(leitura)

                        val sync = SyncLeituraGMF()
                        sync.leituraRepository = leituraRepository
                        sync.loginService = loginService
                        sync.leituraGMFDao = leituraRepository.dataBase.leituraGMFDao()

                        val nomeArquivo = mNomeArquivo.value ?: ""
                        sync.enviarFotosGMF(leitura.numeroLigacao.toString(), leitura.sqLeitura.toString(), nomeArquivo)
                        sync.enviarFotosOcorrGMF(leitura.numeroLigacao.toString(), leitura.sqLeitura.toString(), nomeArquivo)

                        sync.enviarLeituraGMF(leitura, ligacao, mErroDialogCloseMessage, mUpdatedUnidade)

                        uiThread {
                            mUpdatedUnidade.value = ligacao
                        }

                    }
                }, {
                    Log.e("LeituraViewModel", "Error to update $it")
                }).disposedBy(disposeBag)
            }
        }
    }

    fun salvarObservacao(textoObservacao: String) {
        mProgressBarShown.value = true
        val numeroLig = mUnidade.value?.numeroLigacao ?: -1
        mObservacaoDescricao.value = textoObservacao
        val obs = getObservacaoTextoCompleto()
        Log.i("LeituraViewModel", "Unidade Numero Lig: $numeroLig")

        doAsync {
            val leitura = leituraRepository.dataBase.leituraDao().getLeitura(numeroLig).defaultScheduler().blockingFirst()?.get(0)
            if (leitura != null) {
                Log.i("LeituraViewModel", "Preparando para gravar a observacao $obs no registro $numeroLig")
                leituraRepository.gravarObservacao(numeroLig, obs)
                Log.i("LeituraViewModel", "Observacao gravada")
            }
            mProgressBarShown.value = false
        }
    }

    fun finalizarTarefa() {

    }

    fun dismissProcessDialog() {

    }

    fun addImagemLeitura(path: String) {
        if (mImagensLeitura.value == null) {
            mImagensLeitura.value = ArrayList()
        }

        var usuario = leituraRepository?.dataBase?.usuarioDao()?.getUsuario()?.blockingGet()?.get(0)

        mLeituraObject.value?.let { leitura ->

            mNomeArquivo.value?.let { nomeArquivo ->
                val fotoGMF = FotoGMF(null, nomeArquivo, leitura.numeroLigacao.toString(), leitura.sqLeitura.toInt(), montaFotoDSLinha(File(path), leitura, usuario!!), path)
                mImagensLeitura.value?.add(fotoGMF)
                mImagensLeitura.postValue(mImagensLeitura.value)
                leituraRepository.dataBase.fotoGMFDao().insert(fotoGMF)
            }
        }
    }

    fun removeImagemLeitura(fotoGMF: FotoGMF) {
        leituraRepository.dataBase.fotoGMFDao().delete(fotoGMF)
        loadFotos()
    }

    fun loadFotos() {
        doAsync {
            var fotos: List<FotoGMF>? = null
            mLeituraObject.value?.let { leitura ->
                mNomeArquivo.value?.let { nomeArquivo ->
                    fotos = leituraRepository.dataBase.fotoGMFDao()
                            .getFotos(nomeArquivo, leitura.numeroLigacao.toString())
                            .defaultScheduler()
                            .blockingGet()
                }

            }
            mImagensLeitura.postValue(fotos?.toMutableList() ?: mutableListOf())
        }
    }

    @SuppressLint("SimpleDateFormat")
    private fun montaFotoDSLinha(file: File, leitura: Leitura, usuario: Usuario): String {

        val sdf = SimpleDateFormat("yyyyMMdd")
        val stf = SimpleDateFormat("hhmmss")
        val dateFileCreation = Date(file.lastModified())

        val dsLinha = StringBuffer()
        dsLinha.append("SALTOWEB.PRC_COLETOR_SIMU_CEL@SALTOWEBH(''FOTO'', '''', ")
        dsLinha.append("''${nomeArquivo.value}'', ")
        dsLinha.append("''LS01FOTO0100001'', ''")
        dsLinha.append("${leitura.numReg}#")
        dsLinha.append("${leitura.numeroLigacao}#")
        dsLinha.append("${leitura.codigoGrupo}#")
        dsLinha.append("${leitura.mesLancamento}#")
        dsLinha.append("${leitura.anoLancamento}#")
        dsLinha.append("${usuario.codigo}#")
        dsLinha.append("${sdf.format(dateFileCreation)}#")
        dsLinha.append("${stf.format(dateFileCreation)}#")
        dsLinha.append("${file.length().toString().padStart(6, '0')}#")
        dsLinha.append(" #")
        dsLinha.append(montaNomeFoto(leitura))
        dsLinha.append("'')")
        return dsLinha.toString()
    }

    private fun montaNomeFoto(leitura: Leitura): String {

        val nomeFoto = StringBuffer()

        nomeFoto.append("F")
        nomeFoto.append(leitura.codigoGrupo.toString().padStart(2, '0'))
        nomeFoto.append("_")
        nomeFoto.append(leitura.mesLancamento)
        nomeFoto.append(leitura.anoLancamento)
        nomeFoto.append("_")
        nomeFoto.append(leitura.numeroLigacao)
        nomeFoto.append("_")
        nomeFoto.append(leitura.sqLeitura)
        nomeFoto.append(".JPG")
        return nomeFoto.toString()
    }
}