package br.com.iresult.gmfmobile.model.bean

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TarefaFinishResponse(
        @SerializedName("args") val args: Array<String>,
        @SerializedName("headers") val headers: Array<String>,
        @SerializedName("url") val url: String

) : Parcelable {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as TarefaFinishResponse

        if (!args.contentEquals(other.args)) return false
        if (!headers.contentEquals(other.headers)) return false
        if (url != other.url) return false

        return true
    }

    override fun hashCode(): Int {
        var result = args.contentHashCode()
        result = 31 * result + headers.contentHashCode()
        result = 31 * result + url.hashCode()
        return result
    }
}
