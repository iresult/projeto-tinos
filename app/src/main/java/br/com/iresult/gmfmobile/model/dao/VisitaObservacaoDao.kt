package br.com.iresult.gmfmobile.model.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import br.com.iresult.gmfmobile.model.bean.*
import io.reactivex.Observable


@Dao
interface VisitaObservacaoDao {

    @Query("SELECT * FROM VisitaObservacao where numReg = :numReg")
    fun buscaVisita(numReg: String ): Observable<List<VisitaObservacao>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(visitaObservacaos: List<VisitaObservacao>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(visitaObservacao: VisitaObservacao)

    @Query("UPDATE VisitaObservacao SET descricaoObservacao = :descricao WHERE numReg = :numeroRegistro")
    fun gravarObservacao(numeroRegistro: Int, descricao : String) : Int

}
