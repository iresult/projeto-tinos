package br.com.iresult.gmfmobile.ui.main.tarefas.itinerary

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import br.com.iresult.gmfmobile.model.bean.Address
import br.com.iresult.gmfmobile.model.bean.Ligacao
import br.com.iresult.gmfmobile.model.bean.Parametro
import br.com.iresult.gmfmobile.model.bean.Roteiro
import br.com.iresult.gmfmobile.model.database.AppDataBase
import br.com.iresult.gmfmobile.repository.RoteiroRepository
import br.com.iresult.gmfmobile.ui.base.BaseViewModel
import br.com.iresult.gmfmobile.ui.widget.HomeStatus
import br.com.iresult.gmfmobile.utils.TarefaUtils
import br.com.iresult.gmfmobile.utils.disposedBy

/**
 * Created by victorfernandes on 24/02/19.
 */

class RoteiroViewModel(private val repository: RoteiroRepository,  val dataBase: AppDataBase) : BaseViewModel<RoteiroViewModel.State>() {

    enum class State : BaseState

    private val mRoteiros = MutableLiveData<List<Roteiro>>()
    val roteiros: LiveData<List<Roteiro>> = mRoteiros

    private val mRoteiro = MutableLiveData<Roteiro>()
    val roteiro: LiveData<Roteiro> = mRoteiro

    val mTotals = MutableLiveData<HashMap<String, Int>>()
    val totals: LiveData<HashMap<String, Int>> = mTotals

    val mFinalizacaoPreparada = MutableLiveData<HashMap<String, Any>>()
    val finalizacaoPreparada: LiveData<HashMap<String, Any>> = mFinalizacaoPreparada

    private val mAddress = MutableLiveData<List<Address>>()
    val address: LiveData<List<Address>> = mAddress

    private val mTodasLigacoes = MutableLiveData<List<Ligacao>>()

    private val mLigacao = MutableLiveData<List<Ligacao>>()
    val ligacao: LiveData<List<Ligacao>> = mLigacao

    private val mProgress = MutableLiveData<Int>()
    val progess: LiveData<Int> = mProgress

    val mErrorMessage = MutableLiveData<String>()
    val errorMessage: LiveData<String> = mErrorMessage

    fun init() {
        Log.d("RoteiroViewModel", "init()")
        mTodasLigacoes.value = ArrayList()

        repository.getRoteiros().subscribe({
            mRoteiros.value = it
            Log.d("RoteiroViewModel", "Roteiros ${it.size} carregados")
            // calculateTotals()
        }, {
            Log.e("RoteiroViewModel", it.toString())
        }).disposedBy(disposeBag)

        repository.allImoveis().subscribe({ list ->
            mTodasLigacoes.value = list

            Log.d("RoteiroViewModel", "Todas as ${mTodasLigacoes.value?.size} ligações carregadas da lista")
            Log.d("RoteiroViewModel", "Listando as ${mTodasLigacoes.value?.size} ligações")
        }, {
            Log.e("RoteiroViewModel", it.toString())
        }).disposedBy(disposeBag)
    }



//    fun fetchAdressesByFileName(nome: String) {
//        repository.getRoteiro(nome).subscribe({
//            mRoteiro.value = it
//            it.tarefas?.addresses?.let { ruas ->
//                repository.insertRuas(ruas)
//            }
//            mAddress.value = it.tarefas?.addresses
//            this.fetchProgressTask()
//        }, {
//            Log.e("RoteiroViewModel", it.toString())
//        }).disposedBy(disposeBag)
//    }

    fun fetchAdressesByFileName(nome: String) {
        Log.d("RoteiroViewModel", "fetchAdressesByFileName()")
        repository.getRoteiro(nome).subscribe({
            val todasLigacoes = mTodasLigacoes.value
            mTotals.value = TarefaUtils.calculateTotalsByRoteiroLigacoes(it, todasLigacoes as ArrayList<Ligacao>)
            mRoteiro.value = it
            it.tarefas?.addresses?.let { ruas ->
                repository.insertRuas(ruas)
            }
            mAddress.value = it.tarefas?.addresses
            this.fetchProgressTask()
        }, {
            Log.e("RoteiroViewModel", it.toString())
        }).disposedBy(disposeBag)
    }



    private fun fetchProgressTask() {
        repository.allImoveis().subscribe({ list ->
            val totalDone = list.filter { it.filterDone() }.size.toFloat()
            val percentage = (totalDone / list.size.toFloat())
            mProgress.value = (percentage * 100).toInt()
        }, {
            Log.e("RoteiroViewModel", it.toString())
        }).disposedBy(disposeBag)
    }

    fun fetchHouses(streetName: String) {
        mLigacao.value = emptyList()
        repository.fetchImoveis(streetName).subscribe({
            mLigacao.value = it
        }, {
            Log.e("RoteiroViewModel", it.toString())
        }).disposedBy(disposeBag)
    }

//    private fun getLigacoesFromAddress(nome: String): List<Ligacao> {
//        val tempLigacoes: ArrayList<Ligacao> = ArrayList()
//        val listaLigacoes = mTodasLigacoes.value
//        Log.d("RoteiroViewModel", "Procurando ligacoes de $nome em ${mTodasLigacoes.value?.size}")
//        if (listaLigacoes != null) {
//            for (lig in listaLigacoes) {
//                Log.d("RoteiroViewModel", "Ligacao endereco ${lig.enderecoLigacao}")
//                if (lig.rua == nome) {
//                    tempLigacoes.add(lig)
//                }
//            }
//        }
//        return tempLigacoes
//    }

//    private fun calculateTotalsByRoteiro(r: Roteiro) {
//        Log.d("RoteiroViewModel", "calculateTotalsByRoteiro()   Nome: ${r.nome}")
//        val addresses = r.tarefas?.addresses
//        Log.d("RoteiroViewModel", "Enderecos na Tarefa: ${addresses?.size}   Enderecos na Lista: ${mAddress.value?.size}")
//        var casasConcluidas = 0
//        val totalRuas = addresses?.size
//        var totalCasas = 0
//        var totalRuasConcluidas = 0
//        if (addresses != null) {
//            for (addr in addresses) {
//                Log.d("RoteiroViewModel", "Analisando endereco ${addr.nome}")
//                var enderecoConcluido = true
//                val tempLigacoes = getLigacoesFromAddress(addr.nome)
//                Log.d("RoteiroViewModel", "Analisando ligacoes neste endereco ${tempLigacoes.size}")
//                totalCasas += tempLigacoes.size
//                for (ligacao in tempLigacoes) {
//                    Log.i("RoteiroViewModel", "Ligacao: ${ligacao.numero}   Status: ${ligacao.status}   Concluido: ${ligacao.concluido}")
//                    if (ligacao.status != null
//                            && ligacao.status != HomeStatus.AGORA.name
//                            && ligacao.status != HomeStatus.PROXIMO.name) {
//                        casasConcluidas += 1
//                    } else {
//                        enderecoConcluido = false
//                    }
//                }
//                if (enderecoConcluido) {
//                    totalRuasConcluidas += 1
//                }
//            }
//        }
//  }

//    private fun calculateTotals() {
//        Log.d("RoteiroViewModel", "calculateTotals()")
//        mRoteiros.value?.forEach { it ->
//            calculateTotalsByRoteiro(it)
//        }
//    }

//    fun calculateHousesTotal() {
//        Log.d("RoteiroViewModel", "calculateHousesTotal()")
//        if (mRoteiro.value != null) {
//            calculateTotalsByRoteiro(mRoteiro.value!!)
//        }
//        Log.d("RoteiroViewModel", "calculateHousesTotal")
//        // totalEnderecos.value = address.value?.size
//        // mCasasConcluidas.value = ligacao.value?.filter{ it.filterDone() }?.size
//        Log.d("RoteiroViewModel", "Analisando enderecos ${mAddress.value?.size}")
//        mAddress.value?.let { addrs ->
//            {
//                Log.d("RoteiroViewModel", "Enderecos localizados ${addrs.size}")
//                addrs.forEach { addr ->
//                    Log.d("RoteiroViewModel", "Analisando endereco ${addr.nome}")
//                }
//            }
//        }
//    }

    fun updateTotalByRoteiro(rot : Roteiro) {
        val todasLigacoes = mTodasLigacoes.value
        mTotals.value = TarefaUtils.calculateTotalsByRoteiroLigacoes(rot, todasLigacoes as ArrayList<Ligacao>)
    }


    private fun Ligacao.filterDone(): Boolean {
        return this.status != null
                && this.status != HomeStatus.AGORA.name
                && this.status != HomeStatus.PROXIMO.name
    }

    private fun Ligacao.filterNotDone(): Boolean {
        return this.status == null
                || this.status == HomeStatus.AGORA.name
                || this.status == HomeStatus.PROXIMO.name
    }

    fun formattedHouses(ligacoes: List<Ligacao>?, pedding: Boolean, finished: Boolean, reversed: Boolean): List<Ligacao> {
        val worked = ligacoes?.filter { it.filterDone() } ?: emptyList()
        val notWorked = ligacoes?.filter { it.filterNotDone() } ?: emptyList()
        notWorked.firstOrNull()?.let {
            it.status = HomeStatus.AGORA.name
        }
        if (notWorked.size >= 2) {
            notWorked[1].status = HomeStatus.PROXIMO.name
        }
        val completeHouses = worked + notWorked
        val filteredPendding = completeHouses.filter { it.filterNotDone() }
        val filteredFinished = completeHouses.filter { it.filterDone() }

        var finalHouses = mutableListOf<Ligacao>()
        if (!pedding && !finished) {
            finalHouses.addAll(filteredFinished)
            finalHouses.addAll(filteredPendding)
        }

        if (finished) {
            finalHouses.addAll(filteredFinished)
        }

        if (pedding) {
            finalHouses.addAll(filteredPendding)
        }
        finalHouses = finalHouses.sortedBy { it.numReg }.toMutableList()
        if (reversed) {
            finalHouses = finalHouses.asReversed()
        }
        return finalHouses
    }

    fun prepararTarefaParaFinalizacao() : HashMap<String, Any>? {
        Log.i("RoteiroViewModel", "Encerrando a tarefa")
        if (mRoteiro.value != null) {
            val rot : Roteiro = mRoteiro.value!!
            val todasLigacoes = mTodasLigacoes.value
            val totaisRoteiro = TarefaUtils.calculateTotalsByRoteiroLigacoes(rot, todasLigacoes as ArrayList<Ligacao>)
            val parametros = rot.parametros
            val param: Parametro = parametros[0]
            val nomeArquivo = roteiro.value?.nome ?: ""
            val sequencia = roteiro.value?.notificacoes?.size ?: 0
            val concluidas = totaisRoteiro["CASAS_CONCLUIDAS"] ?: 0
            val totais = totaisRoteiro["CASAS_TOTAIS"] ?: 0
            val naoRealizados = totais - concluidas
            val preparacao = HashMap<String, Any>()
            preparacao["SEQUENCIA"] = sequencia
            preparacao["ARQUIVO"] = nomeArquivo
            preparacao["PARAM"] = param
            preparacao["ROTEIRO"] = rot
            preparacao["NAO_REALIZADOS"] = naoRealizados
            preparacao["CASAS_TOTAIS"] = totais
            preparacao["LEITURAS_PENDENTES"] = totaisRoteiro["LEITURAS_PENDENTES"] ?: 0
            preparacao["LEITURAS_ENVIADAS"] = totaisRoteiro["LEITURAS_ENVIADAS"] ?: 0
            return preparacao
        }
        return null
    }
}