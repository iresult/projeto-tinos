package br.com.iresult.gmfmobile.model.bean

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "leituraGMF")
@JsonIgnoreProperties(ignoreUnknown = true)
data class LeituraGMF(
        @PrimaryKey(autoGenerate = true) val uuid: Long?,
        val nomeArquivo: String,
        val sqLeitura: Int,
        val sqLinha: Int,
        val lista: List<String>
) : Parcelable