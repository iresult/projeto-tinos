package br.com.iresult.gmfmobile.model.converters

import androidx.room.TypeConverter
import br.com.iresult.gmfmobile.model.bean.LeituraGMF
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.util.*

class LeituraGMFConverter {

    var gson = Gson()

    @TypeConverter
    fun stringToLeituraGMFList(data: String?): List<LeituraGMF> {
        if (data == null) {
            return Collections.emptyList()
        }

        val listType = object : TypeToken<List<LeituraGMF>>() {}.type

        return gson.fromJson(data, listType)
    }

    @TypeConverter
    fun leituraGMFListToString(leitura: List<LeituraGMF>): String {
        return gson.toJson(leitura)
    }

    @TypeConverter
    fun stringToLeituraGMF(leitura: String?): LeituraGMF {
        return gson.fromJson(leitura, LeituraGMF::class.java)
    }

    @TypeConverter
    fun leituraGMFToString(leitura: LeituraGMF): String {
        return gson.toJson(leitura)
    }
}