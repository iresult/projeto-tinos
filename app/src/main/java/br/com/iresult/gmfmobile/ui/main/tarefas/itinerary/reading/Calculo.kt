package br.com.iresult.gmfmobile.ui.main.tarefas.itinerary.reading

import android.annotation.SuppressLint
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import br.com.iresult.gmfmobile.BuildConfig
import br.com.iresult.gmfmobile.model.Visita
import br.com.iresult.gmfmobile.model.bean.*
import br.com.iresult.gmfmobile.model.database.AppDataBase
import br.com.iresult.gmfmobile.repository.LeituraRepository
import br.com.iresult.gmfmobile.service.LoginService
import br.com.iresult.gmfmobile.utils.defaultScheduler
import org.apache.commons.lang3.StringUtils
import org.jetbrains.anko.doAsync
import java.text.SimpleDateFormat

class Calculo {

    val mClosePrintDialog = MutableLiveData<String>()
    val closePrintDialog: LiveData<String> = mClosePrintDialog

    val mContaCorteDialog = MutableLiveData<String>()
    val montaCorteDialog: LiveData<String> = mContaCorteDialog

    val mShowFormaDeEntregaDialog = MutableLiveData<String>()
    val showFormaDeEntregaDialog: LiveData<String> = mShowFormaDeEntregaDialog

    private val mShowContaRetidaDialog = MutableLiveData<String>()
    val showContaRetidaDialog: LiveData<String> = mShowContaRetidaDialog

    var ligacao: Ligacao? = null
    var leitura: Int = 0
    private var dataBase: AppDataBase? = null
    var imprimeOcorrenciaNotificacao = false;
    var quantidadeEmissao: Int = 0
    var leituraRepository: LeituraRepository? = null
    var loginService: LoginService? = null
    var mUpdatedUnidade = MutableLiveData<Ligacao>()

    var codOcorrencia: Int = 0
    var tiposOcorrencia: List<Ocorrencia>? = null

    constructor(dataBase: AppDataBase, leituraRepository: LeituraRepository, loginService: LoginService, ligacao: Ligacao, leitura: Int, quantidadeEmissao: Int) {
        this.dataBase = dataBase
        this.ligacao = ligacao
        this.leitura = leitura
        this.quantidadeEmissao = quantidadeEmissao
        this.leituraRepository = leituraRepository
        this.loginService = loginService
    }

    fun execute() {

        try {
            var leituraAtual = leitura.toString()
            val leitura = dataBase?.leituraDao()?.getLeitura(ligacao?.numeroLigacao!!)?.defaultScheduler()?.blockingFirst()?.get(0)
            if (quantidadeEmissao >= 1) {
                leituraAtual = leitura?.leituraAtual.toString()
            }
            val visita = Visita(this.dataBase, this.ligacao, leituraAtual)
            visita.quantidadeEmissao = quantidadeEmissao

            if (codOcorrencia != null && codOcorrencia > 0) {
                leitura?.codLeitura = codOcorrencia
                //   if (leitura != null && leitura?.codLeitura!! != null && leitura?.codLeitura!! > 0) {
                visita.codigoLeitura = codOcorrencia // leitura?.codLeitura!!
                visita.tiposOcorrencia = this!!.tiposOcorrencia!!
                //   }
            }

            visita.calculaConsumo()

            visita.calculaFatura()

            visita.atualizarStatus()

            val imprime = visita.verificaRetencao() < 50

            // Força o consumo Faturado de Água para zero nos casos de T.E.E
            if (ligacao?.codigoFaturamento == 3) {
                visita.consumoFaturadoAgua = 0
            }

            // Configura os parâmetros de impressão (faixas de consumo e serviços
            if (imprime) {
                visita.parametrosImpressao(ligacao!!)
            }
//  CascataDAO.getInstance().gravaParametrosImpressao(cascata); // TODO GRAVAR

            val faixas = dataBase!!.faixaDao().cascata(ligacao?.codigoCategoria!!.trim(), ligacao?.codigoFaturamento!!, ligacao?.tipoLigacao!!).defaultScheduler().blockingFirst()

            var qtdFaixa = 0

            for (faixa: Faixa in faixas) {
                qtdFaixa++;
                if (visita.consumoFaturadoAgua.toDouble().compareTo(faixa.consumoMinimo + +0.001) == 1 &&
                        visita.consumoFaturadoAgua.toDouble().compareTo(faixa.consumoMaximo) != 1) {
                    break
                }
            }
            visita.quantidadeFaixas = qtdFaixa

            // Se tiver notificação de débitos, avisa e imprime
            if (imprimeOcorrenciaNotificacao) {
                val notificacao = ligacao?.numeroLigacao?.let { dataBase?.notificacaoDao()?.buscarNotificacao(it) }
                if (notificacao != null && notificacao.isNotEmpty()) {
                    notificacaoDebitos(notificacao)
                    val impNot = ImpressaoNotificacao()
                    impNot.imprime(visita, ligacao!!, notificacao, dataBase!!)
                };
            }


            visita.seqLeitura = dataBase?.leituraDao()?.getMaxSeqLeitura()?.blockingFirst()!! + 1

            atualizaLigacao(visita)
            atualiLeitura(visita, leitura!!)

            if (imprime) {
                if (!ligacao!!.situacaoLigacao.equals("C") && !ligacao!!.situacaoLigacao.equals("K")) {
                    val impressaoConta = ImpressaoConta()
                    impressaoConta.imprimirConta(ligacao, visita, dataBase)
                    mShowFormaDeEntregaDialog.postValue("SHOW")
                } else {
                    mContaCorteDialog.postValue("CORTE")
                }
            } else {
                mShowContaRetidaDialog.postValue(visita.mensagemEntrega)
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    @Throws(Exception::class)
    fun calculaConta() {
        doAsync {
            execute()
        }
    }

    private fun notificacaoDebitos(notificacao: List<Notificacao>?) {

    }


    private fun atualizaLigacao(visita: Visita) {
        ligacao?.statusRegistro = visita.statusRegistro.toString()

        ligacao?.let { dataBase?.ligacaoDao()?.updateLigacao(it) }
    }

    @SuppressLint("SimpleDateFormat")
    private fun atualiLeitura(visita: Visita, leitura: Leitura) {

        if (leitura != null) {

            leitura.leituraAtual = visita.leituraAtual
            leitura.numMat = ligacao?.numeroLigacao!!
            //    leitura.numMat = java.lang.Long.valueOf(dataBase?.usuarioDao()?.getUsuario()?.blockingGet()?.get(0)?.codigo.toString())
            leitura.codLeitura = visita.codigoLeitura
            leitura.codLeituraInterno = visita.codigoLeituraInterno

            val sdf = SimpleDateFormat("yyyyMMdd")
            leitura.dataLeitura = sdf.format(visita.dataLeitura)
            leitura.horaLeitura = visita.horarioLeitura
            leitura.quantidadeTentativas = visita.quantidadeTentativas
            leitura.consumoMedido = visita.consumoMedido
            leitura.consumoMedidoEE = visita.consumoMedidoEsgoto
            leitura.conusumoFaturado = visita.consumoFaturadoAgua
            leitura.conusumoFaturadoEE = visita.consumoFaturadoEsgoto
            leitura.valorTotal = visita.valorTotal
            leitura.qtdCons = visita.quantidadeDiasConsumo
            leitura.criterioFaturamento = visita.criterioFaturamento
            leitura.descStatus = visita.statusVirada + visita.statusRepasse!!
            leitura.descDica = visita.dica
            leitura.credConsumo = visita.creditoConsumo
            leitura.tipoEntrega = visita.tipoEntrega
            leitura.descMotivoEntrega = visita.motivoEntrega
            leitura.qtdEmissoes = visita.quantidadeEmissao + 1
            leitura.quantidadeTentativas = 1
            if (StringUtils.isNotBlank(visita.descricaoObservacao)) {
                leitura.descObs = StringUtils.rightPad(visita.descricaoObservacao, 60 - visita.descricaoObservacao?.length!!, " ")
            }

            leitura.versaoColetor = BuildConfig.VERSION_NAME
            // leitura.setStatusBateria();
            leitura.sqLeitura = visita.seqLeitura
            leitura.valorMinimo = visita.valorMinNaoLancado
            leitura.statusRegistro = visita.statusRegistro
            dataBase?.leituraDao()?.updateLeitura(leitura)
        }
    }
}
