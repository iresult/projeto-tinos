package br.com.iresult.gmfmobile.service

import io.reactivex.Single
import okhttp3.ResponseBody
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import retrofit2.http.Query

interface TarefaService {

    @FormUrlEncoded
    @POST("finalizar_tarefa.php")
    fun finalizarTarefa(@Field("NOM_ARQUIVO") nomeArquivo: String,
                        // @Query("SEQ_LEITURA") sqLeitura: Int,
                        @Field("DS_LINHA") dsLinha: String) : Single<ResponseBody>
}