package br.com.iresult.gmfmobile.model.converters

import androidx.room.TypeConverter
import br.com.iresult.gmfmobile.model.bean.Imei
import br.com.iresult.gmfmobile.model.bean.Ocorrencia
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.util.*

/**
 * Created by victorfernandes on 20/02/19.
 */
class ImeiConverter {

    var gson = Gson()

    @TypeConverter
    fun stringToImeiList(data: String?): List<Imei> {
        if (data == null) {
            return Collections.emptyList()
        }

        val listType = object : TypeToken<List<Imei>>() {

        }.getType()

        return gson.fromJson(data, listType)
    }

    @TypeConverter
    fun imeiListToString(imeis: List<Imei>): String {
        return gson.toJson(imeis)
    }

    @TypeConverter
    fun fromString(value: String): Imei {
        val listType = object : TypeToken<Imei>() {

        }.type
        return Gson().fromJson(value, listType)
    }

    @TypeConverter
    fun imeiToString(imei: Imei): String {
        return Gson().toJson(imei)
    }

}
