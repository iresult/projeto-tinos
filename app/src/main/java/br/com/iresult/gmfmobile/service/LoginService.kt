package br.com.iresult.gmfmobile.service

import br.com.iresult.gmfmobile.model.bean.LoginResponse
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface LoginService {

    @FormUrlEncoded
    @POST("api.php")
    fun login(@Field("usuario") usuario: String, @Field("senha") senha: String): Observable<LoginResponse>


    @FormUrlEncoded
    @POST("save_data.php")
    fun syncLeitura(@Field("NOM_ARQUIVO") nomeArquivo: String,
                    @Field("SEQ_LEITURA") sqLeitura: Int,
                    @Field("SQ_LINHA") sqLinha: Int,
                    @Field("lista[]") lista: List<String>): Observable<Object>

    @FormUrlEncoded
    @POST("save_data.php")
    fun syncLeituraSingle(@Field("NOM_ARQUIVO") nomeArquivo: String,
                          @Field("SEQ_LEITURA") sqLeitura: Int,
                          @Field("SQ_LINHA") sqLinha: Int,
                          @Field("lista[]") lista: List<String>): Single<Object>
}