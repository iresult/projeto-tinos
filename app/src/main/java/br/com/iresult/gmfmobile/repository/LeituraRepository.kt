package br.com.iresult.gmfmobile.repository

import br.com.iresult.gmfmobile.model.dao.LeituraDao
import br.com.iresult.gmfmobile.model.dao.OcorrenciaLeituraDao
import br.com.iresult.gmfmobile.model.dao.RoteiroDao
import br.com.iresult.gmfmobile.model.dao.ServicoDao
import br.com.iresult.gmfmobile.model.database.AppDataBase
import br.com.iresult.gmfmobile.service.FotoService
import io.reactivex.Observable
import io.reactivex.Single
import okhttp3.ResponseBody

class LeituraRepository(private val roteiroDao: RoteiroDao,
                        private val leituraDao: LeituraDao,
                        private val servicoDao: ServicoDao,
                        private val ocorrenciaLeituraDao: OcorrenciaLeituraDao,
                        private val fotoService: FotoService,
                        val dataBase: AppDataBase) {

    fun gravarObservacao(numeroRegistro: Long, descricao: String): Int {
        return leituraDao.gravarObservacao(numeroRegistro, descricao)
    }

    fun sendFotos(nomeArquivo: String, sqLeitura: String, dsLinhas: List<String>, blobs: List<String>): Observable<ResponseBody> {
        return fotoService.sendFotos(nomeArquivo, sqLeitura, dsLinhas, blobs)
    }

    fun getNotificacoesRealizadas(): Single<Long> {
        return dataBase.leituraDao().getEstatisticaNotificacoesRealizadas()
    }

    fun getNotificacoesTotais(): Single<Long> {
        return dataBase.leituraDao().getEstatisticaNotificacoesTotal()
    }
}