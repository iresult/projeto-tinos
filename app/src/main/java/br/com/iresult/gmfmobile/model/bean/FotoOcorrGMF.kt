package br.com.iresult.gmfmobile.model.bean

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "fotoOcorrGMF")
@JsonIgnoreProperties(ignoreUnknown = true)
data class FotoOcorrGMF(
        @PrimaryKey(autoGenerate = true) val uuid: Long?,
        val nomeArquivo: String,
        val numLigacao: String,
        val sqLeitura: Int,
        val dsLinha: String,
        val path: String,
        var resend: Boolean = false
) : Parcelable

data class FotoOcorrItemGMF(
        @SerializedName("DS_LINHA") val dsLinha: String,
        @SerializedName("BLB_FOTO") val blob: ByteArray
)