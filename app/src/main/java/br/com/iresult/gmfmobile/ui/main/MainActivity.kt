package br.com.iresult.gmfmobile.ui.main

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.telephony.TelephonyManager
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.lifecycle.Observer
import br.com.iresult.gmfmobile.R
import br.com.iresult.gmfmobile.ui.base.BaseFragment
import br.com.iresult.gmfmobile.ui.base.SimpleDialog
import br.com.iresult.gmfmobile.ui.login.LoginActivity
import br.com.iresult.gmfmobile.ui.main.impressao.teste.ImpressaoTesteFragment
import br.com.iresult.gmfmobile.ui.main.pesquisa.PesquisaFragment
import br.com.iresult.gmfmobile.ui.main.tarefas.TarefasFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.nav_header_main.view.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class MainActivity : AppCompatActivity() {

    private val MY_PERMISSIONS_REQUEST_READ_CONTACTS: Int = 1005
    val viewModel: MainViewModel by viewModel()
    var selectedButton: View? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        Log.i("MainActivity", "Pesquisando o IMEI")
        fetchIMEINumber()

        viewModel.imei.observe(this, Observer {
            Log.i("MainActivity", "Numero do IMEI Salvo e Alterado no ViewModel, inicializando APP")
            val toggle = ActionBarDrawerToggle(
                    this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
            drawer_layout.addDrawerListener(toggle)
            toggle.drawerArrowDrawable.color = ContextCompat.getColor(this, R.color.colorPrimary)
            toggle.syncState()

            drawer_layout.roteiros.setOnClickListener {
                goToFragment(TarefasFragment(), it)
            }

            // drawer_layout.impressao_backup.setOnClickListener {
            //     goToFragment(ImpressaoBackupFragment(), it)
            // }

            drawer_layout.pesquisa.setOnClickListener {
                goToFragment(PesquisaFragment.newInstance(true), it)
            }


            drawer_layout.impressao_teste.setOnClickListener {
                goToFragment(ImpressaoTesteFragment(), it)
            }

            drawer_layout.sair.setOnClickListener {
                checkForExit(true)
            }


            drawer_layout.roteiros.isSelected = true

            goToFragment(TarefasFragment(), drawer_layout.roteiros)
            setupBindings()

        })


//        val toggle = ActionBarDrawerToggle(
//                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
//        drawer_layout.addDrawerListener(toggle)
//        toggle.drawerArrowDrawable.color = ContextCompat.getColor(this, R.color.colorPrimary)
//        toggle.syncState()

//        drawer_layout.roteiros.setOnClickListener {
//            goToFragment(TarefasFragment(), it)
//        }

       // drawer_layout.impressao_backup.setOnClickListener {
       //     goToFragment(ImpressaoBackupFragment(), it)
       // }

//        drawer_layout.impressao_teste.setOnClickListener {
//            goToFragment(ImpressaoTesteFragment(), it)
//        }
//
//
//
//        drawer_layout.roteiros.isSelected = true
//        drawer_layout.sair.setOnClickListener {
//            viewModel.logout()
//        }
//
//        goToFragment(TarefasFragment(), drawer_layout.roteiros)
//        setupBindings()
    }

    private fun setupBindings() {

        viewModel.getUsuario().observe(this@MainActivity, Observer { usuario ->
            usuario?.let { user ->
                drawer_layout.username.text = "Usuário: ${user.siglaIdentificadora}"
                drawer_layout.usuario.text = user.nome
            }
        })

        viewModel.getState().observe(this@MainActivity, Observer { state ->

            when (state) {
                MainViewModel.State.LOGOUT -> {
                    val intent = Intent(this@MainActivity, LoginActivity::class.java)
                    finish()
                    startActivity(intent)
                }
            }
        })
    }

    private fun goToFragment(fragment: BaseFragment, view: View) {
        val fragmentManager = supportFragmentManager
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame, fragment)
                .commit()
        selectedButton?.let { it.isSelected = false }
        selectedButton = view
        selectedButton?.isSelected = true
        toolbar_title.text = getString(fragment.getTitle())
        drawer_layout.closeDrawer(GravityCompat.START)
    }

    private fun checkForExit(logout : Boolean) {
        var msg = getString(R.string.main_activity_title_dialog_close_app)
        if (logout) {
            msg = getString(R.string.main_activity_title_dialog_logout_close_app)
        }
        SimpleDialog(this, msg).let {
            it.setupActionButton(getString(R.string.yes), getString(R.string.cancel)) {
                it.dismiss()
                if (logout) {
                    viewModel.logout()
                }
                val startMain = Intent(Intent.ACTION_MAIN)
                startMain.addCategory(Intent.CATEGORY_HOME)
                startMain.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(startMain)
                finish()
            }
            it.setDismissListener { it.dismiss() }
        }.show()
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            checkForExit(false)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun fetchIMEINumber() {

        val permission = ActivityCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE)
        if (permission != PackageManager.PERMISSION_GRANTED) {
            Log.i("MainActivity", "Solicitando permissão para READ_PHONE_STATE")
            // Should we show an explanation?
//            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
//                            android.Manifest.permission.READ_PHONE_STATE)) {
//
//                // Show an expanation to the user *asynchronously* -- don't block
//                // this thread waiting for the user's response! After the user
//                // sees the explanation, try again to request the permission.
//
//            } else {
            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.READ_PHONE_STATE),
                    MY_PERMISSIONS_REQUEST_READ_CONTACTS);
            // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
            // app-defined int constant. The callback method gets the
            // result of the request.
//            }
        } else {
            Log.i("MainActivity", "Já possui permissao READ_PHONE_STATE")
            saveIMEINumber()
        }
    }

    @SuppressLint("HardwareIds")
    @Suppress("DEPRECATION")
    fun saveIMEINumber() {
        val permission = ActivityCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE)
        if (permission == PackageManager.PERMISSION_GRANTED) {
            Log.i("MainActivity", "Permissao READ_PHONE_STATE")
            var imeiNumber = "3"
            val telephonyMgr = getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
            val deviceId = telephonyMgr.deviceId
            val softwareVersion = telephonyMgr.deviceSoftwareVersion
            val phoneNumber = telephonyMgr.line1Number
            Log.i("MainActivity", "DeviceID: $deviceId    Software Version: $softwareVersion   Phone Number: $phoneNumber")

            // Para ativar o acesso da Tarefa por IMEI, basta descomentar as linhas abaixo, dentro do IF ELSE
            imeiNumber = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                telephonyMgr.imei
            } else {
                telephonyMgr.deviceId
            }

            val sharedPref = getSharedPreferences("PHONE_INFO", Context.MODE_PRIVATE)
            val editor = sharedPref.edit()
            editor.putString("IMEI", imeiNumber)
            editor.apply()
            Log.i("MainActivity", "IMEI Salvo no PHONE_INFO -> IMEI")
            viewModel.mImei.value = imeiNumber
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            MY_PERMISSIONS_REQUEST_READ_CONTACTS -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.i("MainActivity", "Permissão concedida, salvando o IMEI")
                    saveIMEINumber()

                } else {
                    Log.i("MainActivity", "Permiessão negada")
                    super.onBackPressed()
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return
            }
        }// other 'case' lines to check for other
        // permissions this app might request
    }
}
