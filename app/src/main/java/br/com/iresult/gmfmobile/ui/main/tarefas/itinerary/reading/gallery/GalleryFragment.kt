package br.com.iresult.gmfmobile.ui.main.tarefas.itinerary.reading.gallery

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.iresult.gmfmobile.R
import br.com.iresult.gmfmobile.model.bean.FotoGMF
import br.com.iresult.gmfmobile.ui.PreviewImageActivty
import br.com.iresult.gmfmobile.ui.base.SimpleDialog
import br.com.iresult.gmfmobile.ui.main.tarefas.itinerary.reading.LeituraViewModel
import br.com.iresult.gmfmobile.ui.main.tarefas.itinerary.reading.occourence.CAMERA
import br.com.iresult.gmfmobile.utils.BaseAdapter
import br.com.iresult.gmfmobile.utils.Utils
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_gallery.*
import kotlinx.android.synthetic.main.item_foto.view.*
import org.jetbrains.anko.startActivity
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import pl.aprilapps.easyphotopicker.DefaultCallback
import pl.aprilapps.easyphotopicker.EasyImage
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.EasyPermissions
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException

class GalleryFragment : Fragment() {

    val viewModel: LeituraViewModel by sharedViewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_gallery, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btBack.setOnClickListener { goBack() }
        btFoto.setOnClickListener { cameraTask() }
        setupRecyclerview()
        viewModel.loadFotos()
    }

    private fun goBack() {
        activity?.onBackPressed()
    }

    @SuppressLint("MissingPermission")
    @AfterPermissionGranted(CAMERA)
    fun cameraTask() {

        if (hasCameraPermission()) {
            EasyImage.openCameraForImage(this, 0)
        } else {
            EasyPermissions.requestPermissions(
                    this,
                    "GMF deseja utilizar sua camera",
                    CAMERA,
                    Manifest.permission.CAMERA)
        }
    }

    private fun hasCameraPermission(): Boolean {
        return EasyPermissions.hasPermissions(requireContext(), Manifest.permission.CAMERA)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        EasyImage.handleActivityResult(requestCode, resultCode, data, activity, object : DefaultCallback() {
            override fun onImagePickerError(e: Exception?, source: EasyImage.ImageSource?, type: Int) {
                e?.printStackTrace()
            }

            override fun onImagesPicked(imageFiles: List<File>, source: EasyImage.ImageSource, type: Int) {
                onPhotosReturned(imageFiles)
            }

            override fun onCanceled(source: EasyImage.ImageSource?, type: Int) {
                if (source == EasyImage.ImageSource.CAMERA_IMAGE) {
                    val photoFile = EasyImage.lastlyTakenButCanceledPhoto(requireContext())
                    photoFile?.delete()
                }
            }
        })
    }

    private fun onPhotosReturned(returnedPhotos: List<File>) {
        val path = Utils.saveToInternalStorage(context, returnedPhotos[0], returnedPhotos[0].name)
        viewModel.addImagemLeitura(path)
    }

    private fun setupRecyclerview() {
        recyclerView.layoutManager = GridLayoutManager(context, 3, RecyclerView.VERTICAL, false)
        recyclerView.adapter = BaseAdapter(R.layout.item_foto) { fotoGMF: FotoGMF, itemView ->
            Glide.with(this)
                    .load(fotoGMF.path)
                    .into(itemView.image)

            itemView.image.setOnClickListener {
                val file = File(fotoGMF.path)
                it.context.startActivity<PreviewImageActivty>(PreviewImageActivty.EXTRA_FILE_IMAGE to file)
            }
            itemView.btDelete.setOnClickListener {
                SimpleDialog(itemView.context, "Deseja realmente remover essa imagem?").let { dialog ->
                    dialog.setupActionButton(itemView.context.getString(R.string.yes), itemView.context.getString(R.string.cancel)) {
                        viewModel.removeImagemLeitura(fotoGMF)
                        dialog.dismiss()
                    }.show()
                }
                true
            }
        }.also { adapter ->
            viewModel.imagensLeitura.observe(this, Observer {
                adapter.setItems(it)
            })
        }
    }


}