package br.com.iresult.gmfmobile.model.dao

import androidx.room.*
import br.com.iresult.gmfmobile.model.bean.LeituraGMF
import io.reactivex.Single

@Dao
interface LeituraGMFDao {

    @Query("SELECT * FROM leituraGMF order by uuid ASC")
    fun getLeituras(): Single<List<LeituraGMF>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(leitura: LeituraGMF)

    @Delete
    fun delete(leitura: LeituraGMF)
}
