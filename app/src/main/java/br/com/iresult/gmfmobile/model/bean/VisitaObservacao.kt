package br.com.iresult.gmfmobile.model.bean
import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize


@Entity
@Parcelize
data class VisitaObservacao (
        @PrimaryKey val numReg: String,
        var descricaoObservacao: String?,
        var action: Int
) : Parcelable
