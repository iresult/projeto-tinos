package br.com.iresult.gmfmobile.model.dao

import androidx.room.*
import br.com.iresult.gmfmobile.model.bean.Notificacao
import br.com.iresult.gmfmobile.model.bean.OcorrenciaNotificacao

@Dao
interface OcorrenciaNotificacaoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertNotificacao(vararg notificacao: OcorrenciaNotificacao)

    @Update
    fun updateNotificacao(notificacao: OcorrenciaNotificacao)

    @Delete
    fun deleteNotificacao(notificacao: OcorrenciaNotificacao)

    @Query("SELECT * FROM OcorrenciaNotificacao ")
    fun findAll(): List<OcorrenciaNotificacao>

    @Query("SELECT * FROM OcorrenciaNotificacao WHERE codOcorrencia = :codOcorrencia")
    fun buscarNotificacao(codOcorrencia: String): List<OcorrenciaNotificacao>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(notificacoes: List<OcorrenciaNotificacao>)


}