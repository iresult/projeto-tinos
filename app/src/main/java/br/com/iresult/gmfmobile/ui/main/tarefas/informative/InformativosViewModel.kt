package br.com.iresult.gmfmobile.ui.main.tarefas.informative

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import br.com.iresult.gmfmobile.model.bean.Ligacao
import br.com.iresult.gmfmobile.model.bean.Roteiro
import br.com.iresult.gmfmobile.repository.RoteiroRepository
import br.com.iresult.gmfmobile.ui.base.BaseViewModel
import br.com.iresult.gmfmobile.utils.PreferencesKey
import br.com.iresult.gmfmobile.utils.PreferencesManager
import br.com.iresult.gmfmobile.utils.TarefaUtils
import br.com.iresult.gmfmobile.utils.disposedBy

class InformativosViewModel(private val preferencesManager: PreferencesManager,
                            private val roteiroRepository: RoteiroRepository) : BaseViewModel<InformativosViewModel.State>() {

    enum class State : BaseState

    private val mRoteiro = MutableLiveData<Roteiro>()
    val roteiro: LiveData<Roteiro> = mRoteiro

    fun verifyNotShowAgain(notShowAgain: Boolean) {
        preferencesManager.set(PreferencesKey.NOT_SHOW_INFORMATIVES, notShowAgain)
    }

    fun loadRoteiro(nome: String){
        roteiroRepository.getRoteiros().subscribe({
            it.forEach { roteiro ->
                if (roteiro.nome == nome){
                    mRoteiro.value = roteiro
                }
            }
        }, {
            Log.e("TarefasViewModel", it.toString())
        }).disposedBy(disposeBag)
    }
}