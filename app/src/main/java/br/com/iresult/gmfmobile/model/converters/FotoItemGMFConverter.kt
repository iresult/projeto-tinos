package br.com.iresult.gmfmobile.model.converters

import androidx.room.TypeConverter
import br.com.iresult.gmfmobile.model.bean.FotoItemGMF
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.util.*

class FotoItemGMFConverter {

    var gson = Gson()

    @TypeConverter
    fun stringToFotoItemGMFList(data: String?): List<FotoItemGMF> {
        if (data == null) {
            return Collections.emptyList()
        }

        val listType = object : TypeToken<List<FotoItemGMF>>() {}.type

        return gson.fromJson(data, listType)
    }

    @TypeConverter
    fun fotoItemGMFListToString(fotosItemGMF: List<FotoItemGMF>): String {
        return gson.toJson(fotosItemGMF)
    }

    @TypeConverter
    fun stringToFotoItemGMF(fotoItemGMF: String?): FotoItemGMF {
        return gson.fromJson(fotoItemGMF, FotoItemGMF::class.java)
    }

    @TypeConverter
    fun fotoItemGMFToString(fotoItemGMF: FotoItemGMF): String {
        return gson.toJson(fotoItemGMF)
    }

    @TypeConverter
    fun fromString(value: String): List<String> {
        val listType = object : TypeToken<List<String>>() {}.type
        return gson.fromJson(value, listType)
    }

    @TypeConverter
    fun fromList(list: List<String>): String {
        val json = gson.toJson(list)
        return json
    }
}