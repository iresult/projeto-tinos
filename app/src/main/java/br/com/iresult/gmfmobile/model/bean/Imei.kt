package br.com.iresult.gmfmobile.model.bean

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "imei")
@JsonIgnoreProperties(ignoreUnknown = true)
data class Imei(
        @PrimaryKey
        @SerializedName("NUM_REG") val numReg: Long = 0,
        @SerializedName("COD_IMEI") var codigoImei: String?,
        @SerializedName("END_MAC") var enderecoMac: String?,
        @SerializedName("SIMCARD") var simCard: String?,
        @SerializedName("DAT_ATIV") var dataAtivacao: String?,
        @SerializedName("DAT_BLOQ") var dataBloqueio: String?,
        @SerializedName("DAT_CAD") var dataCadastro: String?
): Parcelable


/*
"IMEI01": [
                {
                    "NUM_REG": "000001",
                    "COD_IMEI": "3",
                    "END_MAC": "33",
                    "SIMCARD": "333",
                    "DAT_ATIV": "20190116",
                    "DAT_BLOQ": "20190301",
                    "DAT_CAD": "20190116"
                }
            ],
 */