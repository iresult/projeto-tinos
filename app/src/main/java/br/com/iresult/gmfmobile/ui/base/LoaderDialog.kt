package br.com.iresult.gmfmobile.ui.base

import android.content.Context
import android.os.Bundle
import br.com.iresult.gmfmobile.R
import kotlinx.android.synthetic.main.simple_dialog.*

class LoaderDialog(context: Context, private val dialogTitle: String? = null) : GmfDialog(context) {

    override fun getLayoutResource(): Int = R.layout.loader_dialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        dialogTitle?.let { title.text = it }

        setCancelable(false)
    }
}