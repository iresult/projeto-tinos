package br.com.iresult.gmfmobile.ui.main.tarefas.itinerary

import android.app.Activity
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.iresult.gmfmobile.R
import br.com.iresult.gmfmobile.model.bean.Address
import br.com.iresult.gmfmobile.model.bean.Ligacao
import br.com.iresult.gmfmobile.ui.base.BaseFragment
import br.com.iresult.gmfmobile.ui.base.SimpleDialog
import br.com.iresult.gmfmobile.ui.main.MainActivity
import br.com.iresult.gmfmobile.ui.main.estatistica.EstatisticaActivity
import br.com.iresult.gmfmobile.ui.main.impressao.Impressao
import br.com.iresult.gmfmobile.ui.main.impressao.ImpressaoDialog
import br.com.iresult.gmfmobile.ui.main.tarefas.itinerary.filter.RoutingFilterDialog
import br.com.iresult.gmfmobile.ui.main.tarefas.itinerary.finish.FinishTaskActivity
import br.com.iresult.gmfmobile.ui.main.tarefas.itinerary.map.MapsHomeActivity
import br.com.iresult.gmfmobile.ui.main.tarefas.itinerary.reading.LeituraActivity
import br.com.iresult.gmfmobile.ui.widget.HomeStatus
import br.com.iresult.gmfmobile.ui.widget.HomeStatusView
import br.com.iresult.gmfmobile.utils.BaseAdapter
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_roteiro.*
import kotlinx.android.synthetic.main.app_bar_roteiro2.*
import kotlinx.android.synthetic.main.rua_item.view.*
import org.jetbrains.anko.startActivity
import org.koin.androidx.viewmodel.ext.android.viewModel


class RoteiroActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    companion object {
        const val PREPARACAO_FINALIZAR_TAREFA_EXTRA = "PREPARACAO_FINALIZAR_TAREFA"
        const val ARG_ROTEIRO = "ARG_ROTEIRO"
        const val REQUEST_CODE_UPDATED_HOUSE = 323
    }

    private var selectedButton: View? = null
    val viewModel : RoteiroViewModel by viewModel()
    private lateinit var noTaskText : TextView
    private var pending: Boolean = true
    private var finished: Boolean = true
    private var reversed: Boolean = false
    private var imprimir: Impressao? = null
    private lateinit var toolbarTitle: TextView
    private lateinit var toolbar: Toolbar
    private val adapter = BaseAdapter(R.layout.rua_item) { address: Address, itemView ->

        itemView.nomeRua.text = address.nome
        itemView.cepRua.text = address.formattedAddress()
        itemView.setOnClickListener {
            this.toggleItem(itemView)
            viewModel.fetchHouses(address.nome)
        }
        itemView.rvHome.layoutManager = GridLayoutManager(itemView.context, 3, RecyclerView.VERTICAL, false)
        itemView.rvHome.adapter = BaseAdapter(R.layout.item_home) { home: Ligacao, homeStatusView ->

            (homeStatusView as HomeStatusView).let { homeView ->
                homeView.tvNumberHome.text = home.numero
                homeView.setupLayout(home.status?.let { status -> HomeStatus.valueOf(status) } ?: run { HomeStatus.NONE })
                homeView.setOnClickListener { this.navigateToReaderActivity(address, home) }
            }

        }.also { adapter ->
            viewModel.ligacao.observe(this, Observer {
                adapter.setItems(viewModel.formattedHouses(it, pending, finished, reversed))
            })
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_roteiro2)
        toolbar = findViewById(R.id.toolbar)
        toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_material)
        setSupportActionBar(toolbar)
        toolbarTitle = findViewById(R.id.toolbar_title1)
        noTaskText = findViewById(R.id.noTaskText)
        viewModel.init()

        intent.extras?.getString(ARG_ROTEIRO)?.let { roteiro ->
            toolbarTitle.text = roteiro
            toolbarTitle.visibility = View.VISIBLE
            viewModel.fetchAdressesByFileName(roteiro)
        }

        ivPreviewMap.setOnClickListener {
            startActivity<MapsHomeActivity>(MapsHomeActivity.EXTRA_BUNDLE_TASK_NAME to toolbarTitle.text.toString())
        }

        btBack.setOnClickListener { finish() }
        setupRecyclerView()

        viewModel.errorMessage.observe(this, Observer { error ->
            val dialog = SimpleDialog(this  , error)
            dialog.setupCloseButton(getString(R.string.ok))
            dialog.show()

        })

        viewModel.roteiro.observe(this, Observer { roteiro ->
            viewModel.updateTotalByRoteiro(roteiro)
            val enderecosConcluidos = viewModel.mTotals.value?.get("RUAS_CONCLUIDAS")
            val totalEnderecos  = viewModel.mTotals.value?.get("RUAS_TOTAIS")
            val casasConcluidas = viewModel.mTotals.value?.get("CASAS_CONCLUIDAS")
            val totalCasas = viewModel.mTotals.value?.get("CASAS_TOTAIS")
            val progress = viewModel.mTotals.value?.get("CASAS_CONCLUIDAS_PERCENT")

            val enderecosFaltantes = if (totalEnderecos != null && enderecosConcluidos != null) { totalEnderecos - enderecosConcluidos }  else  { 0 }
            ruas_total.text = "Endereços ($totalEnderecos)"
            ruas_restantes.text = "Faltam $enderecosFaltantes"

            val casasFaltantes = if (totalCasas != null && casasConcluidas != null) { totalCasas - casasConcluidas }  else  { 0 }
            casas_total.text = "Imóveis ($totalCasas)"
            casas_restantes.text = "Faltam $casasFaltantes"
            porcentagem.text = "$progress%"
        })

        float_roteiro.setOnClickListener {
            RoutingFilterDialog(this, pending, finished, reversed).also { dialog ->
                dialog.onConfirm = { pending, finished, reversed ->
                    this.pending = pending
                    this.finished = finished
                    this.reversed = reversed
                    adapter.setItems(viewModel.address.value ?: emptyList())
                }
            }.show()
        }



        viewModel.progess.observe(this, Observer {
            porcentagem.text = "$it%"
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                progresso.setProgress(it, true)
            } else {
                progresso.progress = it
            }
        })
    }

    override fun onBackPressed() {
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.roteiro2, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_50Itens -> {
                // chamando impressão de 50 itens
                imprimir = Impressao(viewModel.dataBase)
                ImpressaoDialog(this).also { dialog ->
                    dialog.onConfirm = {imprimir50, imprimiBackup ->
                        var impressoraConectada =  true
                        if (imprimir50){
                            impressoraConectada = imprimir?.imprimir50(viewModel.roteiro.value?.nome!!)!!
                        } else if (imprimiBackup) {
                            impressoraConectada = imprimir?.imprimirBackUp(viewModel.roteiro.value?.nome!!)!!
                        }
                        if (!impressoraConectada){
                            viewModel.mErrorMessage.value  = "Impressora não configurada corretamente, favor verificar."
                        }

                    }
                }.show()
            }
            R.id.nav_estatistica -> {
                // Chamando activity Estatisticas
                Log.d("EstatisticaActivity", "Acionando Estatisticas")
                val inten = Intent(this, EstatisticaActivity::class.java)
                val totals = viewModel.totals.value
                inten.putExtra("RUAS_TOTAIS", totals?.get("RUAS_TOTAIS"))
                inten.putExtra("RUAS_CONCLUIDAS", totals?.get("RUAS_CONCLUIDAS"))
                inten.putExtra("CASAS_CONCLUIDAS", totals?.get("CASAS_CONCLUIDAS"))
                inten.putExtra("CASAS_TOTAIS", totals?.get("CASAS_TOTAIS"))
                inten.putExtra("NOTIFICACOES_TOTAIS", totals?.get("NOTIFICACOES_TOTAIS"))
                inten.putExtra("NOTIFICACOES_NAO_ENTREGUES", totals?.get("NOTIFICACOES_NAO_ENTREGUES"))
                val totalRoteiros = totals?.get("RUAS_TOTAIS").toString()
                val lidos = totals?.get("RUAS_CONCLUIDAS").toString()
                val entregues = totals?.get("CASAS_CONCLUIDAS").toString()
                val entregar = totals?.get("CASAS_TOTAIS").toString()
                Log.d("EstatisticaActivity", "Valores inseridos no Extra Ruas Totais: $totalRoteiros  Ruas Concluidas: $lidos  Casas Totais: $entregar    Casas Concluidas: $entregues")
                startActivity(inten)
            }
            R.id.nav_finalizar -> {
                navigateToFinishTask()
            }
            android.R.id.home -> {
                startActivity(Intent(this, MainActivity::class.java))
            }
        }
        return true
    }


    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        startActivity(Intent(this, MainActivity::class.java))
        return true
    }

    private fun goToFragment(fragment: BaseFragment) {

        val fragmentManager = supportFragmentManager
        fragmentManager.beginTransaction()
                .replace(R.id.drawer_layout, fragment)
                .commit()

        selectedButton?.let { it.isSelected = false }
        selectedButton?.isSelected = true
        toolbar_title1.text = getString(fragment.getTitle())
        drawer_layout.closeDrawer(GravityCompat.START)
    }

    private fun resetFilter() {
        this.reversed = false
        this.pending = true
        this.finished = true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(requestCode) {
            REQUEST_CODE_UPDATED_HOUSE -> {
                if (resultCode == Activity.RESULT_OK) {
                    this.resetFilter()
                    viewModel.fetchAdressesByFileName(toolbarTitle.text.toString())
                }
            }
            else -> {

            }
        }
    }

    private fun setupRecyclerView() {
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter.also {
            viewModel.address.observe(this, Observer { addresses ->
                it.setItems(addresses)
                if (addresses.isNotEmpty()) {
                    noTaskText.visibility = View.GONE
                } else {
                    noTaskText.visibility = View.VISIBLE
                }
            })
        }
    }

    private fun toggleItem(view: View) {

        viewModel.address.value?.forEachIndexed { index, _ ->
            val container: View? = recyclerView.getChildAt(index)
            if (container?.isSelected == true && container != view) {
                container.isSelected = false
                container.rvHome?.visibility = View.GONE
            }
        }

        if(view.rvHome.visibility == View.GONE) {
            view.rvHome.visibility = View.VISIBLE
            view.isSelected = true
        } else {
            view.rvHome.visibility = View.GONE
            view.isSelected = false
        }
    }

    private fun navigateToFinishTask() {
        val parametros = viewModel.prepararTarefaParaFinalizacao()
        val intent = Intent(this, FinishTaskActivity::class.java)
        intent.putExtra(PREPARACAO_FINALIZAR_TAREFA_EXTRA, parametros)
        startActivity(intent)
    }

    private fun navigateToReaderActivity(address: Address, home: Ligacao) {
        val intent = Intent(this, LeituraActivity::class.java).let {
            it.putExtra(LeituraActivity.ARG_UNIDADE, home)
            it.putExtra(LeituraActivity.ARG_RUA, address)
            it.putExtra(LeituraActivity.ARG_ROTEIRO, viewModel.roteiro.value?.nome)
        }
        startActivityForResult(intent, REQUEST_CODE_UPDATED_HOUSE)
    }
}
