package br.com.iresult.gmfmobile.ui.main.tarefas.itinerary.reading

import androidx.lifecycle.Observer
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import br.com.iresult.gmfmobile.R
import br.com.iresult.gmfmobile.model.bean.Ligacao
import br.com.iresult.gmfmobile.model.bean.Address
import br.com.iresult.gmfmobile.ui.main.tarefas.itinerary.reading.map.MapHomeActivity
import kotlinx.android.synthetic.main.activity_leitura.*
import org.jetbrains.anko.startActivity
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * Created by victorfernandes on 17/02/19.
 */
class LeituraActivity : AppCompatActivity() {

    val viewModel: LeituraViewModel by viewModel()

    companion object {
        const val ARG_ROTEIRO = "ARG_ROTEIRO"
        const val ARG_UNIDADE = "unidade"
        const val ARG_RUA = "address"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_leitura)

        intent.extras?.apply {
            val address: Address = getParcelable(ARG_RUA) ?: return
            val unidade: Ligacao = getParcelable(ARG_UNIDADE) ?: return
            val routing: String = getString(ARG_ROTEIRO) ?: return
            viewModel.initModel(address, unidade, routing)
        }
        setupBindings()
    }

    fun setupBindings() {

        viewModel.unidade.observe(this, Observer { unidade  ->
            morador.text = unidade.nomeCliente
            status.text = unidade.statusGrupo
            staRegistro()
            numero.text = unidade.numero
            codigo.text = unidade.numReg.toLong().toString()
            endereco.text = viewModel.getFormattedAddress(unidade?.descricaoComplemento)
            hidrometro.text = unidade.numeroMedidor
            matricula.text = String.format("%09d", unidade?.numeroLigacao)

            residencia_label.text = unidade.cat
            format()
        })

        mapa.setOnClickListener {
            this.navigateToMapHome()
        }
    }

    fun format () {
        if (residencia_label.text.toString().equals("R")) {
            residencia_label.text = "RESIDENCIAL"
        } else if (residencia_label.text.toString().equals("I")) {
            residencia_label.text = "Industrial"
        } else if (residencia_label.text.toString().equals("C")) {
            residencia_label.text = "COMERCIAL"
        } else if (residencia_label.text.toString().equals("C1")) {
            residencia_label.text = "COMERCIAL S/HIV"
        } else if (residencia_label.text.toString().equals("O")) {
            residencia_label.text = "OUTROS"
        } else if (residencia_label.text.toString().equals("R1")) {
            residencia_label.text = "RESIDENCIAL SOC"
        } else if (residencia_label.text.toString().equals("P")) {
            residencia_label.text = "PUBLICA"
        } else if (residencia_label.text.toString().equals("00")) {
            residencia_label.text = "----"
        }else if (!residencia_label.text.toString().equals("R")||(!residencia_label.text.toString().equals("I"))
                || (!residencia_label.text.toString().equals("C")) || (!residencia_label.text.toString().equals("C1"))
                || (!residencia_label.text.toString().equals("O"))|| (!residencia_label.text.toString().equals("R1"))
                ||  (!residencia_label.text.toString().equals("P")) ) {
            residencia_label.text = "----"
        }

    }

    fun staRegistro() {

       if (status.text.substring(3, status.length() - 5).equals("A")) {
            status.text = "Ativo"
       } else if (status.text.substring(3, status.length() - 5).equals("C")) {
            status.text = "Cortada"
        } else if (status.text.substring(3, status.length() - 5).equals("K")) {
            status.text = "Corte a pedido"
      }else{
            status.text = "Não Informado"
        }

    }

    private fun navigateToMapHome() {
        val ligacao: Ligacao = this.intent?.extras?.getParcelable(LeituraActivity.ARG_UNIDADE)
                ?: return
        this.startActivity<MapHomeActivity>(MapHomeActivity.EXTRA_LIGACAO to ligacao)
    }
}