package br.com.iresult.gmfmobile.ui.main.tarefas.itinerary.finish

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import br.com.iresult.gmfmobile.R
import br.com.iresult.gmfmobile.model.bean.Roteiro
import br.com.iresult.gmfmobile.ui.base.SimpleDialog
import br.com.iresult.gmfmobile.ui.main.MainActivity
import br.com.iresult.gmfmobile.ui.main.tarefas.itinerary.RoteiroActivity
import kotlinx.android.synthetic.main.activity_transmissao.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class FinishTaskActivity : AppCompatActivity() {

    val viewModel: FinishTaskViewModel by viewModel()
    private var preparacao : HashMap<String, Any>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_transmissao)
        val prep  = intent.extras?.get(RoteiroActivity.PREPARACAO_FINALIZAR_TAREFA_EXTRA) as? HashMap<String, Any>
        Log.i("RoteiroViewModel", "PREPARACAO_FINALIZAR_TAREFA_EXTRA $prep")
        if (prep != null) {
            preparacao = prep
            val casasTotais: Int = prep["CASAS_TOTAIS"] as Int
            val naoRealizados: Int = prep["NAO_REALIZADOS"] as Int
            val realizadas : Int = casasTotais - naoRealizados
            val enviosPendentes : Int = prep["LEITURAS_PENDENTES"] as Int
            val enviosRealizados : Int = prep["LEITURAS_ENVIADAS"] as Int
            lidos_total.text = "$realizadas"
            nao_lidos_total.text = "$naoRealizados"
            envios_pendentes.text = "$enviosPendentes"
            envios_realizados.text = "$enviosRealizados"

            btn_send.setOnClickListener {
                // if (enviosPendentes == 0) {
                if (enviosPendentes == 0 || true) {
                    getActionDialog(this).show()
                } else {
                    getSimpleDialog(this, "Esta tarefa não pode ser finalizada pois ainda possuem ligações pendentes de envio").show()
                }
            }
        }

        btn_back.setOnClickListener { finish() }


        viewModel.finishedTask.observe(this, Observer {
            var msg = "Erro ao encerrar a tarefa por favor tente novamente mais tarde"
            if (it) {
                msg = "Tarefa finalizada com sucesso"
            }
            Log.i("RoteiroViewModel", "Resultado da finalizacao $it")
            getSimpleDialog(this, msg)
            Log.i("RoteiroViewModel", "Fechar pressionado")
            startActivity(Intent(this, MainActivity::class.java))
        })
    }

    private fun getActionDialog(context: Context): SimpleDialog {
        val dialog = SimpleDialog(context, getString(R.string.transmissao_dialog_action_title))
        dialog.setupActionButton(getString(R.string.yes), getString(R.string.cancel)) {
            viewModel.finalizarTarefaPreparada(preparacao)
            dialog.dismiss()
        }
        dialog.setDismissListener {
            // TODO dismiss listener (not called on ok button)
        }
        return dialog
    }

    private fun getSimpleDialog(context: Context, msg : String): SimpleDialog {
        val dialog = SimpleDialog(context, msg)
        dialog.setupCloseButton(getString(R.string.ok))
        return dialog
    }

    private fun getIconDialog(context: Context): SimpleDialog {
        val dialog = SimpleDialog(context, getString(R.string.transmissao_dialog_simple_title))
        dialog.showIcon(true)
        dialog.setupCloseButton(getString(R.string.ok))
        return dialog
    }
}