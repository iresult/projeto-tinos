package br.com.iresult.gmfmobile.ui.main.impressao.teste

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import br.com.iresult.gmfmobile.model.database.AppDataBase
import br.com.iresult.gmfmobile.print.ZebraConnection
import br.com.iresult.gmfmobile.ui.base.BaseViewModel
import br.com.iresult.gmfmobile.ui.main.impressao.Impressao
import br.com.iresult.gmfmobile.ui.main.tarefas.itinerary.RoteiroViewModel
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class ImpressaoTesteViewModel(val dataBase: AppDataBase) : BaseViewModel<RoteiroViewModel.State>() {

    private val mTesteImpressora = MutableLiveData<String>()
    val testeImpressora: LiveData<String> = mTesteImpressora

    enum class State : BaseState

    fun impressaoTeste() {
        if (isImpressoraConectada()) {

            doAsync {
                val imprimir = Impressao(dataBase)
                imprimir.impressaoTeste(object : Impressao.ImpressaoTesteListener {

                    override fun done() {
                        uiThread {
                            mTesteImpressora.value = "Teste realizado com sucesso!"
                        }
                    }
                })
            }
        } else {
            mTesteImpressora.value = "Impressora não configurada corretamente, favor verificar."
        }
    }

    private fun isImpressoraConectada(): Boolean {
        return try {
            (ZebraConnection()).isPortOpen()
        } catch (e: Exception) {
            false
        }
    }
}
