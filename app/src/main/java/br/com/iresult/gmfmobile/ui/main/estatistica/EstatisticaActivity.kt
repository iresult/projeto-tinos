package br.com.iresult.gmfmobile.ui.main.estatistica

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import br.com.iresult.gmfmobile.R
import kotlinx.android.synthetic.main.fragment_estatistica.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import android.util.Log
import kotlinx.android.synthetic.main.item_informative.*

class EstatisticaActivity : AppCompatActivity() {

    val viewModel: EstatisticaViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_estatistica)
        Log.d("EstatisticaActivity", "Lendo dados do bloco extra ${intent.extras?.keySet()}")
        var casasTotais = 0
        var casasConcluidas = 0
        var notificacoesTotais = 0
        var notificacoesNaoEntregues = 0
        intent.extras?.getInt("CASAS_TOTAIS").let {
            if (it != null) {
                casasTotais = it.toInt()
            }
        }

        intent.extras?.getInt("CASAS_CONCLUIDAS").let {
            if (it != null) {
                casasConcluidas = it.toInt()
            }
        }

        intent.extras?.getInt("NOTIFICACOES_TOTAIS").let {
            if (it != null) {
                notificacoesTotais = it.toInt()
            }
        }

        intent.extras?.getInt("NOTIFICACOES_NAO_ENTREGUES").let {
            if (it != null) {
                notificacoesNaoEntregues = it.toInt()
            }
        }

        total_roteiros.text = casasTotais.toString()
        lidos.text = casasConcluidas.toString()

        val naoLidos = casasTotais - casasConcluidas
        nao_lidos.text = naoLidos.toString()
//        notificacoes_entregues.text = notificacoesTotais.toString()
//        nao_entregues.text = notificacoesNaoEntregues.toString()

        Log.d("EstatisticaActivity", "Valores lidos no Totals Casas Totais: ${total_roteiros.text}  Casas Concluidas: ${lidos.text}  Casas Faltantes: ${nao_lidos.text}    Casas Concluidas: ${notificacoes_entregues.text}")

        btn_back.setOnClickListener { finish() }

        viewModel.total_roteiros.observe(this, Observer {
            total_roteiros.setText(it.toString())
        })

        observeNotificacoesRealizadas()
        observeNotificacoesTotais()

        viewModel.getNotificacoesRealizadas()
        viewModel.getNotificacoesTotais()

        //viewModel.updateCount()
    }

    private fun observeNotificacoesRealizadas(){
        viewModel.notificacoesTotais.observe(this, Observer {
            notificacoes_entregues.text = it.toString()
        })
    }

    private fun observeNotificacoesTotais(){
        viewModel.notificacoesRealizadas.observe(this, Observer {
            nao_entregues.text = it.toString()
        })
    }
}