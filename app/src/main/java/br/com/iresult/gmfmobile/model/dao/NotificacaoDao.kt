package br.com.iresult.gmfmobile.model.dao

import androidx.room.*
import br.com.iresult.gmfmobile.model.bean.Notificacao

@Dao
interface NotificacaoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertNotificacao(vararg notificacao: Notificacao)

    @Update
    fun updateNotificacao(notificacao: Notificacao)

    @Delete
    fun deleteNotificacao(notificacao: Notificacao)


    @Query("SELECT * FROM Notificacao WHERE numeroLigacao = :numeroLigacao")
    fun buscarNotificacao(numeroLigacao: Long): List<Notificacao>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(notificacoes: List<Notificacao>)


}