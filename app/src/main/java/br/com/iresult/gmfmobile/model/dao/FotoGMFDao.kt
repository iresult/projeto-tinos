package br.com.iresult.gmfmobile.model.dao

import androidx.room.*
import br.com.iresult.gmfmobile.model.bean.FotoGMF
import io.reactivex.Observable
import io.reactivex.Single

@Dao
interface FotoGMFDao {

    @Query("SELECT * FROM fotoGMF WHERE nomeArquivo = :nomeArquivo AND numLigacao = :numLigacao order by uuid ASC")
    fun getFotos(nomeArquivo: String, numLigacao: String): Single<List<FotoGMF>>

    @Query("SELECT * FROM fotoGMF WHERE resend = 1")
    fun getFotosToResend(): Single<List<FotoGMF>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(fotoGMF: FotoGMF)

    @Delete
    fun delete(fotoGMF: FotoGMF)

    @Update
    fun updateFoto(fotoGMF: FotoGMF)
}
