package br.com.iresult.gmfmobile.ui.main.tarefas.itinerary.reading

import android.util.Log
import br.com.iresult.gmfmobile.BuildConfig
import br.com.iresult.gmfmobile.model.Coleta
import br.com.iresult.gmfmobile.model.Visita
import br.com.iresult.gmfmobile.model.bean.Dominio
import java.util.*
import br.com.iresult.gmfmobile.model.bean.Ligacao
import br.com.iresult.gmfmobile.model.bean.Notificacao
import br.com.iresult.gmfmobile.model.database.AppDataBase
import br.com.iresult.gmfmobile.print.RW420PrintManager
import br.com.iresult.gmfmobile.print.RW420PrintManager.ARIAL_06
import br.com.iresult.gmfmobile.print.RW420PrintManager.ARIAL_09
import br.com.iresult.gmfmobile.utils.Utils
import org.apache.commons.lang3.StringUtils
import java.text.DecimalFormat
import java.text.SimpleDateFormat

class ImpressaoNotificacao {

    private val MAX_NOTIF = 14

    private val TAM_MAX_ENDERECO = 55

    var tarefa: Coleta? = null

    private var datLeitura: String? = null

    private var datVencimentoNotif: Date? = null

    private var receita: String? = null

    val df = DecimalFormat("###,##0.00")
    // pula uma linha de tamanho normal
    val pulaLinha = 16


    // Barcode
    var barcode = arrayOfNulls<String>(4)
    var digitoBarcode = arrayOfNulls<String>(4)


    fun imprime(tarefa: Visita, ligacao: Ligacao, notificacoes : List<Notificacao>, dataBase : AppDataBase){

        this.tarefa = tarefa.tarefa
        var sdf = SimpleDateFormat("dd/MM/yyyy")
        this.datLeitura = sdf.format(tarefa.dataLeitura)

        // Data de Vencimento da Notificação (10 dias a contar da data de leitura)


        val calendar = Calendar.getInstance()
        calendar.add(Calendar.DAY_OF_MONTH, 10)
        this.datVencimentoNotif = calendar.time


        val vertical = intArrayOf(790, 725, 670, 620, 575, 520, 470, 420, 375, 330, 270)
        val linha = intArrayOf(0, 140, 230, 300, 410, 450, 560, 790, 1049, 1049, 1340)
        val slip = intArrayOf(1539, 1609, 1674, 1694)
        val box = 1990


        var corte = 0
        var valor = 0.0

        for(notificacao  in notificacoes){
            valor = valor + notificacao.valFat!!
        }
        val dom : Dominio = dataBase.dominioDao().getDominio("DA_CODFATUR", ligacao.codigoFaturamento.toString())
        if (dom != null) {
            receita = ligacao.codigoFaturamento.toString()  + " - " + dom.descricao
        } else {
            receita =  ligacao.codigoFaturamento.toString() + "-NAO CLASSIFICADA"
        }

        montarBarCodeNotif(ligacao)

        val pm = RW420PrintManager()
        if (pm.isPortOpen()) {

            pm.setPageConfig(340, 1);

            pm.setTone(-15);
            pm.setSpeed(0);

            pm.setPageWidth(110);

            // Estes dois métodos fazem funcionar o black mark
            pm.setLabel();
            pm.setBarSense();

            //  pm.printImage(200, 240, notificacao);


            /**### 0º Linha ###**/

            pm.printText(RW420PrintManager.ARIAL_06, RW420PrintManager.FONT_SIZE_0, 730,
                    linha[0] + 15, BuildConfig.VERSION_NAME);


            /**### 1º Linha ###**/

            // ----> Ligação
            pm.printText(RW420PrintManager.ARIAL_06, RW420PrintManager.FONT_SIZE_0, 40,
                    linha[1], ligacao.ligacaoDigito?.trim());


            // ----> Referência
            pm.printText(RW420PrintManager.ARIAL_06, RW420PrintManager.FONT_SIZE_0, 240, linha[1], "Notificacao");


            // ----> Vencimento
            sdf = SimpleDateFormat("dd/MM/yyyy")
            pm.printText(RW420PrintManager.ARIAL_06, RW420PrintManager.FONT_SIZE_0, 410,
                    linha[1], sdf.format(datVencimentoNotif));


            // ----> Total a pagar


            var totalPagar = df.format(valor)
            pm.printText(RW420PrintManager.ARIAL_09, RW420PrintManager.FONT_SIZE_0, 600,
                    linha[1], StringUtils.leftPad(totalPagar, 12, ' '))


            /**### 2º Linha ###**/
            // ----> Nome
            pm.printText(RW420PrintManager.ARIAL_06, RW420PrintManager.FONT_SIZE_0, 40, linha[2],
                    ligacao.nomeCliente);


            /**### 3º Linha ###**/

            // ----> Endereço
            var sizeStr = ligacao.enderecoLigacao?.length
            if (sizeStr != null) {
                if (sizeStr < TAM_MAX_ENDERECO) {

                    pm.printText(ARIAL_06, RW420PrintManager.FONT_SIZE_0, 40,
                            linha[3], ligacao.enderecoLigacao);

                } else {

                    corte = Utils.Companion.getNextSpaceIndex(ligacao.enderecoLigacao,
                            TAM_MAX_ENDERECO);

                    pm.printText(ARIAL_06, RW420PrintManager.FONT_SIZE_0, 40,
                            linha[3], ligacao.enderecoLigacao?.substring(0, corte));

                    linha[3] += pulaLinha;

                    pm.printText(ARIAL_06, RW420PrintManager.FONT_SIZE_0, 40,
                            linha[3], ligacao.enderecoLigacao?.substring(corte));

                }
            }


            /**### 4º Linha ###**/
            // ----> RECEITA
            pm.printText(ARIAL_06, RW420PrintManager.FONT_SIZE_0, 150,
                    linha[4], receita);

            // ----> Economias
            pm.printText(ARIAL_06, RW420PrintManager.FONT_SIZE_0, 550,
                    linha[4], ligacao.totalEconomias.toString());

            /**### 5º Linha ###**/
            // ----> Nº Hidrômetro
            pm.printText(ARIAL_06, RW420PrintManager.FONT_SIZE_0, 150,
                    linha[5], ligacao.numeroMedidor);

            val categoria: String
            // ----> Categoria
            if (ligacao.codigoCategoria?.substring(0, 1).equals("R")) {
                categoria = "RESIDENCIAL"
            } else if (ligacao.codigoCategoria?.substring(0, 1).equals("C")) {
                categoria = "COMERCIAL"
            } else if (ligacao.codigoCategoria?.substring(0, 1).equals("I")) {
                categoria = "INDUSTRIAL"
            } else if (ligacao.codigoCategoria?.substring(0, 1).equals("P")) {
                categoria = "PUBLICO"
            } else {
                categoria = "OUTROS"
            }

            pm.printText(ARIAL_06, RW420PrintManager.FONT_SIZE_0, 550,
                    linha[5], categoria);


            /**### 6º Linha ###**/
            // ----> Faixa de consumos
            linha[6] += pulaLinha;
            pm.printText(ARIAL_06, RW420PrintManager.FONT_SIZE_0, 240, linha[6],
                    "*** NOTIFICACAO DE DEBITOS ***");


            /**### 7º Linha ###**/
            var msg = StringBuffer()
            msg.append("PREZADO CLIENTE,")
            msg.append("\n")
            msg.append("DEVIDO AOS DEBITOS INFORMADOS AO LADO, SUA")
            msg.append("\n")
            msg.append("LIGACAO FOI INCLUIDA EM PROCESSO DE CORTE, E TERA")
            msg.append("\n")
            msg.append("O FORNECIMENTO DE AGUA SUSPENSO EM 10 DIAS A ")
            msg.append("\n")
            msg.append("CONTAR DESTA DATA.")
            msg.append("\n")
            msg.append("PARA EVITAR O CORTE, PROVIDENCIE O PAGAMENTO DO")
            msg.append("\n")
            msg.append("PRESENTE, OU COMPARECA AO ATENDIMENTO DA")
            msg.append("\n")
            msg.append("SANESALTO. (HORARIOS NO VERSO)")


            pm.printMultiLineText(pulaLinha, ARIAL_06, RW420PrintManager.FONT_SIZE_0, 30,
                    linha[7], msg.toString());

            // ### DISCRIMINAÇÃO DO FATURAMENTO ###
            // -----------------------------------------------------
            pm.printLine(565, linha[7], 565, 998, 1)

            val debitos = this.montaFaturamento(ligacao.numeroLigacao, notificacoes)


            //430
            for (x in 0 until debitos.size) { //520
                if (x == MAX_NOTIF) {
                    pm.printText(ARIAL_06, RW420PrintManager.FONT_SIZE_0, 500, linha[7], debitos[x])
                } else {
                    pm.printText(ARIAL_06, RW420PrintManager.FONT_SIZE_0, 570, linha[7], debitos[x])
                }

                linha[7] += pulaLinha
            }


            /**### 8º Linha ###**/
            msg = StringBuffer()
            msg.append("CASO JA TENHA REGULARIZADO SUA SITUACAO, FAVOR DESCONSIDERAR")
            msg.append("\n")
            msg.append("ESSA NOTIFICACAO.")
            msg.append("\n")
            msg.append("\n")
            msg.append("\n")
            msg.append("                ****** Ligacao Passivel de Corte ******")

            pm.printMultiLineText(pulaLinha, ARIAL_06, RW420PrintManager.FONT_SIZE_0, 30,
                    linha[8], msg.toString())


            if (this.tarefa?.staEmiteBoletoNotif.equals("S") &&
                    ( this.tarefa?.valLimiteBoletoNotif?.equals(0.0)!! ||
                            valor > this.tarefa?.valLimiteBoletoNotif!! ) ){


                /**
                 * **************** Slip da Conta ****************
                 */

                pm.printText(ARIAL_06, RW420PrintManager.FONT_SIZE_0,
                        40, (slip[0]-68), BuildConfig.VERSION_NAME);

                /**### 0º Linha ###**/
                // ----> Ligação
                pm.printText(ARIAL_06, RW420PrintManager.FONT_SIZE_0, 40,
                        slip[0], ligacao.ligacaoDigito?.trim());


                // ----> Referência
                pm.printText(ARIAL_06, RW420PrintManager.FONT_SIZE_0, 240, slip[0],
                        (StringUtils.rightPad( this.tarefa?.mesMedicao.toString(), 2,  "0") + "/" + this.tarefa?.mesMedicao.toString()));

                // Vencimento
                pm.printText(ARIAL_06, RW420PrintManager.FONT_SIZE_0,
                        410, slip[0], datVencimentoNotif.toString());


                // ----> Valor do documento
                pm.printText(ARIAL_09, RW420PrintManager.FONT_SIZE_0,
                        600, slip[0], StringUtils.rightPad(df.format(valor), 12, ' '));


                /**### 1º Linha ###**/
                // ----> Data do Documento
                pm.printText(ARIAL_06, RW420PrintManager.FONT_SIZE_0,
                        300, slip[1], datVencimentoNotif.toString());

                /**### 2 e 3º Linha ###**/
                pm.printText(RW420PrintManager.FONT_TYPE_MONACO, RW420PrintManager.FONT_SIZE_0, 70,
                        slip[2], this.barcode[0] + "-"
                        + this.digitoBarcode[0] + " "
                        + this.barcode[1] + "-"
                        + this.digitoBarcode[1] + " "
                        + this.barcode[2] + "-"
                        + this.digitoBarcode[2] + " "
                        + this.barcode[3] + "-"
                        + this.digitoBarcode[3] );

                // ----> Código de barras (1,2,100,10,1650)
                pm.printBarCode("I2OF5", 1, 2, 110, 50, slip[3], barcode[0]
                        + barcode[1] + barcode[2]
                        + barcode[3]);
            } else {

                pm.printText(ARIAL_06, RW420PrintManager.FONT_SIZE_0,
                        40, (slip[0]-68), BuildConfig.VERSION_NAME);

                pm.printText(ARIAL_09, RW420PrintManager.FONT_SIZE_0, 190, slip[2],
                        "** Notificacao de Debitos **");

                slip[3] += pulaLinha;
                // Emitir frase no corpo do boleto
                pm.printText(ARIAL_09, RW420PrintManager.FONT_SIZE_0,
                        320, slip[3], "SEM EFEITO");
            }

            pm.setPageFinalize();
            pm.print();
            pm.disconnect()

        }
    }

    fun montaFaturamento(numeroLigacao: Long, faturamento: List<Notificacao>  ): Array<String?> {

        //		String[] linhas = (faturamento != null) ? new String[this
        //				.calculaLinhas(faturamento.length)] : null;
        val linhas = arrayOfNulls<String>(15)
        var i = 0
        var outrosDebitos = 0.0
        val espaco = Utils.Companion.space(1)
        val passou = false

        if (faturamento != null) {

            // As strings criadas ficam com o valor (string) "null" armazenadas,
            // tem que limpar
            for (t in linhas.indices) {
                linhas[t] = ""
            }

            // armazena todas descrições
            val descricao = arrayOfNulls<String>(faturamento!!.size)

            // coloca todas as discriminações da fatura
            // dentro do array descrição[y]
            for (y in faturamento!!.indices) {

                descricao[y] =  Utils.Companion.setField(faturamento!!.get(y).ref, 7, 1) + espaco
                descricao[y] += Utils.Companion.setField(df.format(faturamento!!.get(y).valFat) , 15, 0) + espaco

            }

            for (x in descricao.indices) {

                // vai zerar a linha (montar a 2ª coluna) em duas condições:
                // 1 - chegou ao tamanho limite das linhas
                // 2 - chegou a 12 linhas e tem outros débitos
                //				if (x == linhas.length || x == 12) {
                //					if (!passou) {
                //						i = 0;
                //						passou = true;
                //					}
                //				}

                // se chegar ao final de 12 linhas, a 13ª TEM que ser 'outros
                // débitos'
                // e sai do loop
                //				if (x == 24) {
                if (x == MAX_NOTIF) {

                    for (z in x until descricao.size) {
                        outrosDebitos += faturamento!!.get(z).valFat!!
                    }

                    linhas[i] = "Outros Debitos" +  Utils.Companion.setField(
                            df.format(outrosDebitos ), 15, 0)
                    break
                }

                // coloca a descrição dentro da linha
                linhas[i] += descricao[x]

                i++

            }

        }

        return linhas

    }

    protected fun montarBarCodeNotif(ligacao : Ligacao) {

        // RECUPERA O NUMERO DO CÓDIGO DE BARRA.
        val codigoBarra = ligacao.codigoBarrasNotificacao

        // ADICIONA O VALOR DA FATURA NO CODIGO DE BARRA.
        if (StringUtils.isNotBlank( codigoBarra) ) {

            // MONTA A LINHA DE NÚMEROS DO CÓDIGO DE BARRA.
            this.formataCodDigCon(codigoBarra!!)
        }

    }


    @Throws(Exception::class)
    protected fun montaBarCode(ligacao: Ligacao, valorConta: String, valorTotal: Double) {

        val virgula: Int
        val inteiros: String
        val centavos: String
        val valorSomadoString: String
        val valorTotalFatura: String

        virgula = valorConta.indexOf(",")
        inteiros = valorConta.substring(0, virgula).replace(".", "")
        centavos = valorConta.substring(virgula + 1, valorConta.length)
        // isso forma uma String, não é soma!
        valorSomadoString = inteiros.replace(".", "") + centavos

        //Formata o valor total da fatura com as 10 casas decimais a esquerda.
        valorTotalFatura = StringUtils.leftPad(valorSomadoString, 11, "0")


        //Recupera o numero do código de barra.
        var codigoBarra = ligacao.codigoBarras


        //Adiciona o valor da fatura no codigo de barra.
        if (codigoBarra != null && codigoBarra!!.length > 0) {

            codigoBarra = codigoBarra!!.substring(0, 4) + valorTotalFatura + codigoBarra!!.substring(15, codigoBarra!!.length)

            //Monta a linha de números do código de barra.
            this.formataCodDigCon(codigoBarra)

            //Verificação do código de barras
            val total = java.lang.Double.parseDouble(valorSomadoString) / 100

            //Aqui sim é uma soma
            if (java.lang.Double.parseDouble(inteiros + centavos) / 100 != total) {
                Log.e("ImpressaoConta", "ERRO NO CODIGO DE BARRAS")
                return
            }

            if (Integer.parseInt(valorSomadoString) != Integer.parseInt(codigoBarra!!.substring(5, 15))) {
                Log.e("ImpressaoConta", "ERRO NO CODIGO DE BARRAS")
                return
            }
        }

    }

    protected fun formataCodDigCon(sCod: String) {
        var sCod = sCod

        sCod = calculoModulo10QuartoDig(sCod)

        if (sCod.length != 44)
            return

        this.barcode[0] = sCod.substring(0, 11)
        this.barcode[1] = sCod.substring(11, 22)
        this.barcode[2] = sCod.substring(22, 33)
        this.barcode[3] = sCod.substring(33, 44)

        this.digitoBarcode[0] = Utils.calcularDV(this.barcode[0]!!).toString()

        this.digitoBarcode[1] = Utils.calcularDV(this.barcode[1]!!).toString()

        this.digitoBarcode[2] = Utils.calcularDV(this.barcode[2]!!).toString()

        this.digitoBarcode[3] = Utils.calcularDV(this.barcode[3]!!).toString()

    }

    private fun calculoModulo10QuartoDig(sCod: String): String {
        var sCod = sCod
        val sb44 = sCod.substring(0, 3) + sCod.substring(4, 44)
        val sDv44 = Utils.calcularDV(sb44).toString()
        sCod = sCod.substring(0, 3) + sDv44 + sCod.substring(4, 44)
        return sCod
    }
}