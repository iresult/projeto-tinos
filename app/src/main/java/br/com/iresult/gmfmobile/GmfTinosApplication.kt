package br.com.iresult.gmfmobile

import android.content.Context
import android.util.Base64
import android.util.Log
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
import br.com.iresult.gmfmobile.koin.Modules
import br.com.iresult.gmfmobile.repository.LeituraRepository
import br.com.iresult.gmfmobile.service.LoginService
import br.com.iresult.gmfmobile.ui.main.tarefas.itinerary.reading.SyncLeituraGMF
import br.com.iresult.gmfmobile.utils.Utils
import br.com.iresult.gmfmobile.utils.defaultScheduler
import br.com.iresult.gmfmobile.utils.disposedBy
import com.facebook.stetho.Stetho
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.apache.commons.lang3.exception.ExceptionUtils
import org.koin.android.ext.android.inject
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import java.util.*


/**
 * Created by victorfernandes on 14/10/18.
 */
class GmfTinosApplication : MultiDexApplication() {

    private val leituraRepository: LeituraRepository by inject()
    private val loginService: LoginService by inject()
    private val leituraGMFTimer = Timer()
    private val fotoGMFTimer = Timer()
    private val fotoOcorrGMFTimer = Timer()
    private val sync = SyncLeituraGMF()

    private val disposeBag = CompositeDisposable()

    override fun onTerminate() {
        super.onTerminate()
        disposeBag.dispose()
    }

    override fun onCreate() {
        super.onCreate()
        Stetho.initializeWithDefaults(this)
        startKoin {
            androidContext(this@GmfTinosApplication)
            modules(Modules.all)
        }

        leituraGMFTimer.scheduleAtFixedRate(LeituraGMFTimerTask(), 0, 10000) // 1 min
        fotoGMFTimer.scheduleAtFixedRate(FotoGMFTimerTask(), 0, 10000) // 1 min
        fotoOcorrGMFTimer.scheduleAtFixedRate(FotoOcorrGMFTimerTask(), 0, 10000) // 1 min
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    inner class LeituraGMFTimerTask : TimerTask() {

        override fun run() {

            Log.e("LeituraGMFTask", "EXECUTANDO SYNC TASK")
            leituraRepository.dataBase.leituraGMFDao().getLeituras()
                    .defaultScheduler()
                    .subscribe({ leituras ->
                        if (leituras.isNotEmpty()) {
                            val leitura = leituras[0]
                            Log.e("LeituraGMFTask", "TENTANDO DAR SYNC, ID: ${leitura.uuid}")
                            loginService.syncLeitura(leitura.nomeArquivo, leitura.sqLeitura, leitura.sqLinha, leitura.lista)
                                    .defaultScheduler()
                                    .subscribe({
                                        leituraRepository.dataBase.leituraGMFDao().delete(leitura)
                                        Log.e("LeituraGMFTask", "CONSEGUIU DAR SYNC, ID: ${leitura.uuid}")
                                        if (leituras.size > 1) {
                                            this.run()
                                        }
                                    }, {
                                        Log.e("LeituraGMFTask", "NÃO CONSEGUIU DAR SYNC, TENTANDO OUTRA VEZ")
                                    })
                                    .disposedBy(disposeBag)
                        }
                    }, {
                        Log.e("LeituraGMFTask", "NÃO CONSEGUIU LER LEITURAS PENDENTES", it)
                    })
                    .disposedBy(disposeBag)
        }
    }

    inner class FotoGMFTimerTask : TimerTask() {

        override fun run() {

            Log.e("FotoGMFTimerTask", "EXECUTANDO SYNC TASK")
            leituraRepository.dataBase.fotoGMFDao().getFotosToResend()
                    .defaultScheduler()
                    .blockingGet()
                    ?.let { fotosDb ->

                        if (fotosDb.isNotEmpty()) {

                            val foto = fotosDb[0]
                            val numeroLigacao = foto.numLigacao
                            val sqLeitura = foto.sqLeitura.toString()
                            val nomeArquivo = foto.nomeArquivo

                            leituraRepository.dataBase.fotoGMFDao()
                                    .getFotos(nomeArquivo, numeroLigacao)
                                    .defaultScheduler()
                                    .blockingGet()
                                    ?.let { fotos ->
                                        if (fotos.isNotEmpty()) {
                                            val dsLinhas = fotos.map { it.dsLinha }
                                            val blobs = fotos.map { Base64.encodeToString(Utils.getBlobFromPath(it.path), Base64.DEFAULT) }
                                            leituraRepository.sendFotos(nomeArquivo, sqLeitura, dsLinhas, blobs)
                                                    .observeOn(AndroidSchedulers.mainThread())
                                                    ?.subscribeOn(Schedulers.io())?.subscribe({
                                                        Log.e("FotoGMFTimerTask", "CONSEGUIU DAR SYNC, N. FOTOS: ${fotos.size}")
                                                        fotos.forEach { foto ->
                                                            foto.resend = false
                                                            leituraRepository.dataBase.fotoGMFDao().updateFoto(foto)
                                                        }
                                                        if (fotosDb.size > 1) {
                                                            this.run()
                                                        }
                                                    }, {
                                                        Log.e("sendFotos Error", ExceptionUtils.getStackTrace(it))
                                                    })
                                                    ?.disposedBy(disposeBag)
                                        }
                                    }
                        }

                    }
        }
    }

    inner class FotoOcorrGMFTimerTask : TimerTask() {

        override fun run() {

            Log.e("FotoOcorrGMFTimerTask", "EXECUTANDO SYNC TASK")
            leituraRepository.dataBase.fotoOcorrGMFDao().getFotosOcorrToResend()
                    .defaultScheduler()
                    .blockingGet()
                    ?.let { fotosDb ->

                        if (fotosDb.isNotEmpty()) {

                            val foto = fotosDb[0]
                            val numeroLigacao = foto.numLigacao
                            val sqLeitura = foto.sqLeitura.toString()
                            val nomeArquivo = foto.nomeArquivo

                            leituraRepository.dataBase.fotoOcorrGMFDao()
                                    .getFotosOcorr(nomeArquivo, numeroLigacao)
                                    .defaultScheduler()
                                    .blockingGet()
                                    ?.let { fotos ->
                                        if (fotos.isNotEmpty()) {
                                            val dsLinhas = fotos.map { it.dsLinha }
                                            val blobs = fotos.map { Base64.encodeToString(Utils.getBlobFromPath(it.path), Base64.DEFAULT) }
                                            leituraRepository.sendFotos(nomeArquivo, sqLeitura, dsLinhas, blobs)
                                                    .observeOn(AndroidSchedulers.mainThread())
                                                    ?.subscribeOn(Schedulers.io())?.subscribe({
                                                        Log.e("FotoOcorrGMFTimerTask", "CONSEGUIU DAR SYNC, N. FOTOS: ${fotos.size}")
                                                        fotos.forEach { foto ->
                                                            foto.resend = false
                                                            leituraRepository.dataBase.fotoOcorrGMFDao().updateFoto(foto)
                                                        }
                                                        if (fotosDb.size > 1) {
                                                            this.run()
                                                        }
                                                    }, {
                                                        Log.e("sendFotosOcorr Error", ExceptionUtils.getStackTrace(it))
                                                    })
                                                    ?.disposedBy(disposeBag)
                                        }
                                    }
                        }

                    }
        }
    }

}