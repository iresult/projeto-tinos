package br.com.iresult.gmfmobile.ui.main.tarefas

import android.content.Context.MODE_PRIVATE
import android.content.Intent
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import br.com.iresult.gmfmobile.R
import br.com.iresult.gmfmobile.model.bean.Imei
import br.com.iresult.gmfmobile.model.bean.Roteiro
import br.com.iresult.gmfmobile.ui.base.BaseFragment
import br.com.iresult.gmfmobile.ui.main.estatistica.EstatisticaActivity
import br.com.iresult.gmfmobile.ui.main.tarefas.informative.InformativosActivity
import br.com.iresult.gmfmobile.ui.main.tarefas.itinerary.RoteiroActivity
import br.com.iresult.gmfmobile.utils.BaseAdapter
import br.com.iresult.gmfmobile.utils.TarefaUtils
import kotlinx.android.synthetic.main.fragment_tarefas.*
import kotlinx.android.synthetic.main.roteiro_item.view.*
import org.jetbrains.anko.startActivity
import org.koin.androidx.viewmodel.ext.android.viewModel

class TarefasFragment : BaseFragment() {

    val viewModel: TarefasViewModel by viewModel()

    override fun getLayoutResource(): Int = R.layout.fragment_tarefas

    override fun getTitle(): Int = R.string.tarefas_title

    override fun setupView() {
        val sharedPreferences = context?.getSharedPreferences("PHONE_INFO", MODE_PRIVATE)
        val imeiValue = sharedPreferences?.getString("IMEI", "")

        if (imeiValue != null && imeiValue != "") {
            Log.i("TarefasFragment", "IMEI Localizado: $imeiValue")
            Toast.makeText(context, "IMEI localizado $imeiValue", Toast.LENGTH_LONG).show()
            Log.i("TarefasFragment", "RecyclerView: $recyclerView")
            recyclerView.let {
                it.layoutManager = LinearLayoutManager(context)
                it.adapter = BaseAdapter<Roteiro>(R.layout.roteiro_item) { roteiro, itemView ->
                    val enderecosFaltantes = viewModel.mTotalEndereco.value!! - viewModel.mEnderecosConcluidos.value!!
                    val casasFaltantes = viewModel.mTotalCasas.value!! - viewModel.mCasasConcluidas.value!!

                    itemView.nomeArquivo.text = roteiro.nome
                    itemView.ruas_total.text = "Endereços (${viewModel.mTotalEndereco.value})"
                    itemView.ruas_restantes.text = "Faltam $enderecosFaltantes"
                    itemView.casas_total.text = "Imóveis (${viewModel.mTotalCasas.value})"
                    itemView.casas_restantes.text = "Faltam $casasFaltantes"
                    itemView.porcentagem.text = "${viewModel.mProgress.value}%"
                    itemView.ivStatistics.setOnClickListener {
                        val inten = Intent(context, EstatisticaActivity::class.java)
                        val totals = viewModel.totals.value
                        inten.putExtra("RUAS_TOTAIS", totals?.get("RUAS_TOTAIS"))
                        inten.putExtra("RUAS_CONCLUIDAS", totals?.get("RUAS_CONCLUIDAS"))
                        inten.putExtra("CASAS_CONCLUIDAS", totals?.get("CASAS_CONCLUIDAS"))
                        inten.putExtra("CASAS_TOTAIS", totals?.get("CASAS_TOTAIS"))
                        inten.putExtra("NOTIFICACOES_TOTAIS", totals?.get("NOTIFICACOES_TOTAIS"))
                        inten.putExtra("NOTIFICACOES_NAO_ENTREGUES", totals?.get("NOTIFICACOES_NAO_ENTREGUES"))
                        inten.putExtra("TOTALS", viewModel.totals.value)
                        startActivity(inten)
                    }
                    itemView.setOnClickListener {
                        if (viewModel.notShowInformatives()) {
                            context?.startActivity<RoteiroActivity>(RoteiroActivity.ARG_ROTEIRO to roteiro.nome)
                        } else {
                            context?.startActivity<InformativosActivity>(InformativosActivity.ARG_ROTEIRO_NOME to roteiro.nome)
                        }
                    }
                }.also { adapter ->
                    viewModel.roteiros.observe(this, Observer { roteiro ->
                        val roteiroApproved = ArrayList<Roteiro>()
                        Log.i("TarefasFragment", "Analisando os roteiros")
                        roteiro.forEach { rot ->
                            val imeis = rot.imeis as MutableList<Imei>
                            // imeis.add( Imei(1, "352514095028415", "1123123", "123123", "20190901",  "20300101", "20190101"))
                            // val imeisIds = imeis?.mapIndexed { imeiIndex, imeiItem ->
                            //     "$imeiIndex  - ${imeiItem.codigoImei} \n"
                            // }
                            // Log.i("TarefasFragment", "IMEI Lista: $imeisIds")
                            // Toast.makeText(context, "Lista de Imeis recebidos $imeisIds)", Toast.LENGTH_LONG).show()
                            if (imeis != null) {
                                val imeisInLista = TarefaUtils.findImeiInImeiList(imeiValue, imeis)
                                val imeiApproved = imeisInLista >= 0
                                val statusApproved = rot.status != 'F'
                                val approvedForToday = TarefaUtils.roteiroApprovedForToday(rot)
                                val approved = imeiApproved && approvedForToday && statusApproved
                                if (approved) {
                                    roteiroApproved.add(rot)
                                } else if (!imeiApproved) {
                                    Toast.makeText(context, "IMEI não cadastrado para a tarefa ${rot.nome} ", Toast.LENGTH_LONG).show()
                                }
                                // Toast.makeText(context, "Tarefa Aprovada: $approved   O IMEI atual na lista de imeis: $imeisInLista   Aprovado para hoje: $approvedForToday  Lista de Imeis ($imeis)", Toast.LENGTH_LONG).show()
                                // Log.i("TarefasFragment", "Tarefa Aprovada: $approved   IMEI na Lista: $imeiApproved   Data Aprovada: $approvedForToday   Status Aprovado: $statusApproved")
                                // Toast.makeText(context, "Tarefa Aprovada: $approved   IMEI na Lista: $imeiApproved   Data Aprovada: $approvedForToday   Status Aprovado: $statusApproved", Toast.LENGTH_LONG).show()
                            }
                        }
                        adapter.setItems(roteiroApproved)
                    })
                }
            }
        } else {
            Log.i("TarefasFragment", "IMEI Não disponível: $imeiValue")
            Toast.makeText(context, "IMEI deste dispositivo não está disponível", Toast.LENGTH_LONG).show()
        }
    }
}