package br.com.iresult.gmfmobile.ui.main.tarefas.itinerary.finish

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import br.com.iresult.gmfmobile.model.bean.Parametro
import br.com.iresult.gmfmobile.model.bean.Roteiro
import br.com.iresult.gmfmobile.model.database.AppDataBase
import br.com.iresult.gmfmobile.repository.RoteiroRepository
import br.com.iresult.gmfmobile.ui.base.BaseViewModel
import br.com.iresult.gmfmobile.ui.main.tarefas.itinerary.RoteiroViewModel
import br.com.iresult.gmfmobile.utils.disposedBy
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class FinishTaskViewModel(private val repository: RoteiroRepository, val dataBase: AppDataBase) : BaseViewModel<RoteiroViewModel.State>() {

    private var mFinishedTask = MutableLiveData<Boolean>()
    val finishedTask : LiveData<Boolean>  = mFinishedTask

    fun finalizarTarefaPreparada(preparacao : HashMap<String, Any>?) {
        Log.i("FinishTaskViewModel", "Finalizando a tarefa")
        if (preparacao != null) {
            val param: Parametro = preparacao["PARAM"] as Parametro
            val nomeArquivo: String = preparacao["ARQUIVO"] as String
            // val sequencia: Int = preparacao["SEQUENCIA"] as Int
            val rot : Roteiro? = preparacao["ROTEIRO"] as Roteiro?
            val naoRealizados: Int = preparacao["NAO_REALIZADOS"] as Int
            doAsync {
                var resultado = false
                repository.finalizarTarefa(nomeArquivo, param, rot, naoRealizados)
                        .subscribe({
                            val response = it.string()
                            val success = response.indexOf("1", 0, false) == 2
                            Log.i("RoteiroViewModel", "Sucesso: $success Localizado Valor 1 na posicao: [${response.indexOf("1", 0, false)}]")
                            resultado = success
                            if (success) {
                                repository.desativarRoteiro(nomeArquivo)
                                Log.i("RoteiroViewModel", "Tarefa $nomeArquivo foi finalizada")
                            } else {
                                Log.i("RoteiroViewModel", "Tarefa $nomeArquivo não foi finalizada")
                            }
                        }, {
                            val resp = it.message
                            Log.i("RoteiroViewModel", "Erro ao encerrar tarefa $resp")
                            Log.e("RoteiroViewModel", "Stack trace", it)
                            resultado = false
                        }).disposedBy(disposeBag)
                uiThread { currModel ->
                    currModel.mFinishedTask.value = resultado
                }
            }
        }
    }
}