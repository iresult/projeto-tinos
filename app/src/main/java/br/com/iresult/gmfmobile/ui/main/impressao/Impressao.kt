package br.com.iresult.gmfmobile.ui.main.impressao

import br.com.iresult.gmfmobile.BuildConfig
import br.com.iresult.gmfmobile.model.bean.Leitura
import br.com.iresult.gmfmobile.model.database.AppDataBase
import br.com.iresult.gmfmobile.print.RW420PrintManager
import br.com.iresult.gmfmobile.print.RW420PrintManager.ARIAL_06
import br.com.iresult.gmfmobile.print.RW420PrintManager.ARIAL_09
import br.com.iresult.gmfmobile.utils.Utils
import org.apache.commons.lang3.StringUtils


class Impressao {

    private val pm = RW420PrintManager()

    private var dataBase: AppDataBase? = null

    constructor(dataBase: AppDataBase) {
        this.dataBase = dataBase
    }

    fun imprimir50(coletor: String): Boolean {

        if (pm.isPortOpen) {
            val leituras: List<Leitura> = dataBase?.leituraDao()?.getLeituras50()?.blockingFirst()!!
            if (!leituras.isEmpty()) {

                Thread(Runnable {
                    imprimir(leituras, coletor)

                }).start()
                return true
            }
        }

        return false
    }


    fun imprimirBackUp(coletor: String): Boolean {

        if (pm.isPortOpen) {
            val leituras: List<Leitura> = dataBase?.leituraDao()?.getLeiturasBackup()?.blockingFirst()!!
            if (!leituras.isEmpty()) {

                Thread(Runnable {
                    imprimir(leituras, coletor)

                }).start()
                return true
            }
        }

        return false
    }

    private fun imprimir(leituras: List<Leitura>, coletor: String) {

        var dCA: Int
        var dCB: Int
        var dCE: Int
        var dCD: Int = 0
        var dCC: Int
        var dCF: Int

        pm.setPageConfig(340, 1)
        pm.setLabel()
        pm.setContrast(0)
        pm.setTone(0)
        pm.setSpeed(5)
        pm.setPageWidth(110)
        pm.setBarSense()

        var y = 100 // coordenada y (linha de impressão) //550
        var nCont = 0
        leituras.forEach { leitura ->


            // var nLinhas = 0 // Quantidade de Ligações impressas


            if (nCont == 0) {

                y = 132

                // 10
                pm.printText(ARIAL_06,
                        RW420PrintManager.FONT_SIZE_0, 40, y,
                        "Coletor: " + coletor)

                y = y + 32

                // 10
                pm.printText(ARIAL_06,
                        RW420PrintManager.FONT_SIZE_0, 30, y,
                        "         CA")

                // 173
                pm.printText(ARIAL_06,
                        RW420PrintManager.FONT_SIZE_0, 193, y,
                        "       CB")

                // 313
                pm.printText(ARIAL_06,
                        RW420PrintManager.FONT_SIZE_0, 333, y,
                        "  CE")

                // 390
                pm.printText(ARIAL_06,
                        RW420PrintManager.FONT_SIZE_0, 410, y,
                        "  CD")

                // 470
                pm.printText(ARIAL_06,
                        RW420PrintManager.FONT_SIZE_0, 490, y,
                        "   CC")

                // 640
                pm.printText(ARIAL_06,
                        RW420PrintManager.FONT_SIZE_0, 660, y,
                        "   CF")


                y = y + 16 // pulando 1 linha

                pm.printText(ARIAL_06,
                        RW420PrintManager.FONT_SIZE_0, 30, y,
                        "-----------")

                pm.printText(ARIAL_06,
                        RW420PrintManager.FONT_SIZE_0, 193, y,
                        "---------")

                pm.printText(ARIAL_06,
                        RW420PrintManager.FONT_SIZE_0, 333, y,
                        "----")

                pm.printText(ARIAL_06,
                        RW420PrintManager.FONT_SIZE_0, 410, y,
                        "----")

                pm.printText(ARIAL_06,
                        RW420PrintManager.FONT_SIZE_0, 490, y,
                        "-----")

                pm.printText(ARIAL_06,
                        RW420PrintManager.FONT_SIZE_0, 570, y,
                        "-----")

                pm.printText(ARIAL_06,
                        RW420PrintManager.FONT_SIZE_0, 660, y,
                        "-----")

                pm.printText(ARIAL_06,
                        RW420PrintManager.FONT_SIZE_0, 760,
                        y, "--")

                y = y + 16 // pulando 1 linha

            }

            nCont++

            dCA = leitura.numeroLigacao.toInt() + 9875
            dCB = leitura.leituraAtual + 245

            var value = 0
            if (StringUtils.isNotBlank(leitura.descMotivoEntrega)) {
                value = leitura.descMotivoEntrega?.substring(0, 2)?.toInt()!!
            }
            dCE = value * 7

            if (StringUtils.isNotBlank(leitura.tipoEntrega)) {
                value = leitura.tipoEntrega?.toInt()!!
                dCD = value + 18
            }

            dCC = leitura.codLeitura * 9
            dCF = leitura.numReg.toInt() + 199

            pm.printText(ARIAL_06, RW420PrintManager.FONT_SIZE_0,
                    30, y, Utils.Companion.setField(dCA, 11))

            pm.printText(ARIAL_06, RW420PrintManager.FONT_SIZE_0,
                    193, y, Utils.Companion.setField(dCB, 9))

            pm.printText(ARIAL_06, RW420PrintManager.FONT_SIZE_0,
                    333, y, Utils.Companion.setField(dCE, 4))

            pm.printText(ARIAL_06, RW420PrintManager.FONT_SIZE_0,
                    410, y, Utils.Companion.setField(dCD, 4))

            pm.printText(ARIAL_06, RW420PrintManager.FONT_SIZE_0,
                    490, y, Utils.Companion.setField(dCC, 5))

            pm.printText(ARIAL_06, RW420PrintManager.FONT_SIZE_0,
                    660, y, Utils.Companion.setField(dCF, 5))

            y = y + 32


        }

        pm.setPageFinalize()
        pm.print()

        pm.setPageConfig(340, 1)
        pm.setLabel()
        pm.setContrast(0)
        pm.setTone(0)
        pm.setSpeed(5)
        pm.setPageWidth(110)
        pm.setBarSense()
    }


    fun impressaoTeste(listener: ImpressaoTesteListener) {

        Thread(Runnable {
            imprimirTeste()
            listener.done()
        }).start()
    }

    interface ImpressaoTesteListener {
        fun done()
    }


    /**
     * Realiza a impressão de teste
     *
     * @return Booleano que indica se a impressão foi realizada com sucesso
     * @throws Exception
     */
    @Throws(Exception::class)
    private fun imprimirTeste(): Boolean {

        var modelo: String = "MODELO.pcx";
        var impresso = false

        // pula uma linha de tamanho normal
        var pulaLinha: Int = 16;


        /*
		 * Verifica a conexão com a impressora
		 */
        if (pm.isPortOpen()) {

            pm.setPageConfig(340, 1)
            pm.setContrast(0)
            pm.setTone(0)
            pm.setSpeed(3)
            pm.setPageWidth(110)

            /* Estes dois métodos fazem funcionar o black mark */
            pm.setLabel()
            pm.setBarSense()

            // pm.printImage(200, 600, modelo)
            // pm.printImage(200, 1490, modelo)

            //			int linha[] = { 0, 100, 130, 180, 255, 320, 375, 580, 650, 720, 850 };
            val linha = intArrayOf(0, 140, 230, 300, 410, 450, 560, 710, 790, 1049, 1340)
            //			final int faixa[] = { 30, 125, 220, 310, 430, 630 };

            val slip = intArrayOf(1555, 1620, 1700, 1720)

            //			final int box = 1990;

            pm.printText(ARIAL_06, RW420PrintManager.FONT_SIZE_0, 730,
                    linha[0] + 15, BuildConfig.VERSION_NAME)

            // ### 1º Linha ###
            // ----> Ligação
            pm.printText(ARIAL_06, RW420PrintManager.FONT_SIZE_0, 40,
                    linha[1], "999999999-9")
            // -----------------------------------------------------

            // ----> Referência
            pm.printText(ARIAL_06, RW420PrintManager.FONT_SIZE_0, 240, linha[1],
                    "99/9999")

            // ----> Vencimento
            pm.printText(ARIAL_06, RW420PrintManager.FONT_SIZE_0, 410,
                    linha[1], "99/99/9999")

            pm.printText(ARIAL_09, RW420PrintManager.FONT_SIZE_0, 600,
                    linha[1],
                    Utils.Companion.space(12 - "9.999,99".length) + "9.999,99")

            // ### 2º Linha ###
            // -----------------------------------------------------

            //
            //			// ----> Inscr. Cadastral
            //			pm.printText(ARIAL_06, RW420PrintManager.FONT_SIZE_0, 700,
            //						linha[1], "99999-9999");


            // ----> Nome
            pm.printText(ARIAL_06, RW420PrintManager.FONT_SIZE_0, 40, linha[2],
                    "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")

            // ### 3º Linha ###
            // -----------------------------------------------------

            // ----> Endereço
            pm.printText(ARIAL_06, RW420PrintManager.FONT_SIZE_0, 40,
                    linha[3], "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")

            linha[3] += pulaLinha

            pm.printText(ARIAL_06, RW420PrintManager.FONT_SIZE_0, 40,
                    linha[3], "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")


            // ### 4º Linha ###
            // -----------------------------------------------------

            // ----> RECEITA
            pm.printText(ARIAL_06, RW420PrintManager.FONT_SIZE_0, 150,
                    linha[4], "XXXX E XXXXXXXX")

            // ----> Economias
            pm.printText(ARIAL_06, RW420PrintManager.FONT_SIZE_0, 550,
                    linha[4], "999")

            // ### 5º Linha ###
            // -----------------------------------------------------
            // ----> Nº Hidrômetro
            pm.printText(ARIAL_06, RW420PrintManager.FONT_SIZE_0, 150,
                    linha[5], "XXXXXXXXXX")

            //CATEGORIA
            pm.printText(ARIAL_06, RW420PrintManager.FONT_SIZE_0, 550,
                    linha[5], "XXXXXXXXXX")


            // ### 6º Linha ###
            // -----------------------------------------------------
            // ----> Faixa de consumos

            pm.printText(ARIAL_06, RW420PrintManager.FONT_SIZE_0, 40, linha[6],
                    "99/9999  9999    9999    99/99/9999  99/9999  9999    9999    99/99/9999")
            linha[6] += pulaLinha
            pm.printText(ARIAL_06, RW420PrintManager.FONT_SIZE_0, 40, linha[6],
                    "99/9999  9999    9999    99/99/9999  99/9999  9999    9999    99/99/9999")
            linha[6] += pulaLinha
            pm.printText(ARIAL_06, RW420PrintManager.FONT_SIZE_0, 40, linha[6],
                    "99/9999  9999    9999    99/99/9999  99/9999  9999    9999    99/99/9999")


            // ### 7º Linha ###
            // -----------------------------------------------------
            // ----> Data da Leitura
            pm.printText(ARIAL_06, RW420PrintManager.FONT_SIZE_0, 30,
                    linha[7], "99/99/9999")

            //CONDICAO
            pm.printText(ARIAL_06, RW420PrintManager.FONT_SIZE_0, 180,
                    linha[7], "XXXXXXX")

            // ----> Leitura Atual
            pm.printText(ARIAL_06, RW420PrintManager.FONT_SIZE_0, 300, linha[7],
                    "9999999")

            // ----> Consumo Medido/m³
            pm.printText(ARIAL_06, RW420PrintManager.FONT_SIZE_0, 375, linha[7],
                    "9999999")

            // ----> Consumo Faturado/m³
            pm.printText(ARIAL_06, RW420PrintManager.FONT_SIZE_0, 470, linha[7],
                    "9999999")

            // ----> Dias de Consumo
            pm.printText(ARIAL_06, RW420PrintManager.FONT_SIZE_0, 590,
                    linha[7], "999")

            // ----> Próxima Leitura
            pm.printText(ARIAL_06, RW420PrintManager.FONT_SIZE_0, 680, linha[7],
                    "99/99/9999")

            /* Espaço para impressão das faixas de Consumo */
            // Impressão das Faixas de Consumo + Lançamentos
            // 11 faixas + 1 faixa (linha do tratamento Sanesalto)
            for (i in 0..10) {

                pm.printText(ARIAL_06, RW420PrintManager.FONT_SIZE_0, 30,
                        linha[8], "XXXXXXXXXXXXXXXXX")
                pm.printText(ARIAL_06, RW420PrintManager.FONT_SIZE_0, 650,
                        linha[8], "          99,99")

                linha[8] += pulaLinha
            }


            for (i in 0..9) {
                pm.printText(ARIAL_06, RW420PrintManager.FONT_SIZE_0, 53,
                        linha[9], "XXXXXXXXXX")
                pm.printText(ARIAL_06, RW420PrintManager.FONT_SIZE_0, 239,
                        linha[9], "9,9999")
                pm.printText(ARIAL_06, RW420PrintManager.FONT_SIZE_0, 363,
                        linha[9], "9,9999")
                pm.printText(ARIAL_06, RW420PrintManager.FONT_SIZE_0, 485,
                        linha[9], "9,9999")
                pm.printText(ARIAL_06, RW420PrintManager.FONT_SIZE_0, 603,
                        linha[9], "9,9999")

                linha[9] += 24
            }

            pm.appendNewLine("BOX 40 996 700 1288 1")
            pm.appendNewLine("T 5 0 518 1001 RESIDENCIAL")
            pm.appendNewLine("T Ari06Bpt.cpf 0 614 1025 TOTAL")
            pm.appendNewLine("T Ari06Bpt.cpf 0 44 1000 CALCULO DO SEU CONSUMO")
            pm.appendNewLine("T Ari06Bpt.cpf 0 250 1025 AGUA")
            pm.appendNewLine("T Ari06Bpt.cpf 0 362 1025 ESGOTO")
            pm.appendNewLine("T Ari06Bpt.cpf 0 464 1025 TRATAMENTO")
            pm.appendNewLine("BOX 40 1118 700 1144 1")
            pm.appendNewLine("LINE 335 622 337 624 1")
            pm.appendNewLine("LINE 88 272 90 274 1")
            pm.appendNewLine("T Ari06Bpt.cpf 0 88 1024 FAIXA")
            //			pm.appendNewLine("T Ari06Bpt.cpf 0 53 1048 até 10");
            //			pm.appendNewLine("T Ari06Bpt.cpf 0 53 1073 de 10 a 15");
            //			pm.appendNewLine("T Ari06Bpt.cpf 0 239 1049 1,9940");
            //			pm.appendNewLine("T Ari06Bpt.cpf 0 239 1073 2,2300");
            pm.appendNewLine("LINE 207 1023 207 1289 1")
            pm.appendNewLine("LINE 330 1023 330 1287 1")
            pm.appendNewLine("LINE 576 1023 576 1289 1")
            pm.appendNewLine("LINE 453 1023 453 1289 1")
            pm.appendNewLine("BOX 40 1022 700 1047 1")
            pm.appendNewLine("BOX 40 1046 700 1071 1")
            //pm.appendNewLine("IL 41 997 698 997 48")
            pm.appendNewLine("BOX 40 1263 700 1288 1")
            pm.appendNewLine("BOX 40 1094 700 1119 1")
            pm.appendNewLine("BOX 40 1143 700 1168 1")
            pm.appendNewLine("BOX 40 1167 700 1192 1")
            pm.appendNewLine("BOX 40 1191 700 1216 1")
            pm.appendNewLine("BOX 40 1215 700 1240 1")
            pm.appendNewLine("BOX 40 1239 700 1264 1")


            //MARCAR LINHA
            pm.appendNewLine("IL 42 1072 698 1072 21")

            // ***********************************************
            // **************** Slip da Conta ****************
            // ***********************************************
            pm.printText(ARIAL_06, RW420PrintManager.FONT_SIZE_0,
                    40, slip[0] - 72, Utils.Companion.setField("V." + BuildConfig.VERSION_NAME, 20, 0))

            // ### 0º Linha ###
            // ----> Ligação
            pm.printText(ARIAL_06, RW420PrintManager.FONT_SIZE_0, 40,
                    slip[0], "999999999-9")
            // -----------------------------------------------------

            // ----> Referência
            pm.printText(ARIAL_06, RW420PrintManager.FONT_SIZE_0, 240, slip[0],
                    "99/9999")

            // ----> Vencimento
            pm.printText(ARIAL_06, RW420PrintManager.FONT_SIZE_0, 410,
                    slip[0], "99/99/9999")

            pm.printText(ARIAL_09, RW420PrintManager.FONT_SIZE_0, 600,
                    slip[0],
                    Utils.Companion.space(12 - "9.999,99".length) + "9.999,99")

            // ### 1º Linha ###
            //----> Data do Documento
            pm.printText(ARIAL_06, RW420PrintManager.FONT_SIZE_0,
                    300, slip[1], "99/99/9999")

            // ### 2º Linha ###
            //----> Linha digitavel
            pm.printText(ARIAL_06, RW420PrintManager.FONT_SIZE_0,
                    40, slip[2], "99999999999999999999999999999999999999999999")

            // ### 3º Linha ###
            // * Espaço para impressão do código de barras
            pm.printLine(40, slip[3], 800, slip[3], 15)

            pm.setPageFinalize()
            pm.print()
            pm.disconnect()

            impresso = true

        } else {
            impresso = false
        }

        return impresso

    }
}
