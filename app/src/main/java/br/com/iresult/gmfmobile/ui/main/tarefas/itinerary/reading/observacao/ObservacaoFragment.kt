package br.com.iresult.gmfmobile.ui.main.tarefas.itinerary.reading.observacao

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.navigation.fragment.NavHostFragment
import br.com.iresult.gmfmobile.R
import br.com.iresult.gmfmobile.ui.main.tarefas.itinerary.reading.LeituraViewModel
import kotlinx.android.synthetic.main.activity_leitura.*
import kotlinx.android.synthetic.main.fragment_observacao.*
import kotlinx.android.synthetic.main.fragment_ocorrencia.btBack
import kotlinx.android.synthetic.main.fragment_ocorrencia.btCancel
import kotlinx.android.synthetic.main.fragment_ocorrencia.btSave
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

const val CAMERA = 1

class ObservacaoFragment : androidx.fragment.app.Fragment() {

    val viewModel : LeituraViewModel by sharedViewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val lo = inflater.inflate(R.layout.fragment_observacao, container, false)
        return lo
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btBack.setOnClickListener {
            // NavHostFragment.findNavController(this).navigate(R.id.menu)
            // parentFragment?.childFragmentManager?.popBackStack()
            goBack()
        }
        btCancel.setOnClickListener {
            ligObservacao.setText("")
            goBack()
        }

        btSave.setOnClickListener {
            viewModel.salvarObservacao(ligObservacao.text.toString())
            goBack()
        }

        setupBindings()
    }

    private fun goBack() {
        // NavHostFragment.findNavController(this).navigate(R.id.menu)
        //NavHostFragment.findNavController(this).popBackStack()
        activity?.onBackPressed()
    }

    private fun setupBindings() {
        viewModel.observacaoDescricao.observe(viewLifecycleOwner, Observer {
            if (it != ligObservacao.text.toString()) {
                ligObservacao.setText(it)
                View.GONE
            }
        })

        viewModel.progressBarShown.observe(viewLifecycleOwner, Observer {
            if (leituraProgressBar!= null) {
                leituraProgressBar.visibility = if (it) View.VISIBLE else View.GONE
            }
        })
    }
}