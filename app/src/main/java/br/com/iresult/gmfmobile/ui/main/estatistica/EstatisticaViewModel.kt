package br.com.iresult.gmfmobile.ui.main.estatistica

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import br.com.iresult.gmfmobile.repository.LeituraRepository
import br.com.iresult.gmfmobile.ui.base.BaseViewModel
import br.com.iresult.gmfmobile.ui.main.pesquisa.PesquisaViewModel
import br.com.iresult.gmfmobile.utils.disposedBy

class EstatisticaViewModel(private val leituraRepository: LeituraRepository) : BaseViewModel<PesquisaViewModel.State>() {

    private val mTag = javaClass.name

    val total_roteiros = MutableLiveData<Int>()

    private val mNotificacoesRealizadas = MutableLiveData<Long>()
    val notificacoesRealizadas: LiveData<Long> = mNotificacoesRealizadas

    private val mNotificacoesTotais = MutableLiveData<Long>()
    val notificacoesTotais: LiveData<Long> = mNotificacoesTotais

    fun getNotificacoesRealizadas() {
        leituraRepository.getNotificacoesRealizadas()
                .subscribe({
                    mNotificacoesRealizadas.value = it
                }, {
                    Log.e(mTag, it.message, it)
                })
                .disposedBy(disposeBag)
    }

    fun getNotificacoesTotais() {
        leituraRepository.getNotificacoesTotais()
                .subscribe({
                    mNotificacoesTotais.value = it
                }, {
                    Log.e(mTag, it.message, it)
                })
                .disposedBy(disposeBag)
    }
}
