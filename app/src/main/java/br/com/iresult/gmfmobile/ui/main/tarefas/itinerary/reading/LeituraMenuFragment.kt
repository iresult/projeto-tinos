package br.com.iresult.gmfmobile.ui.main.tarefas.itinerary.reading

import android.app.Activity
import android.app.AlertDialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.NavHostFragment
import br.com.iresult.gmfmobile.R
import br.com.iresult.gmfmobile.model.Visita
import br.com.iresult.gmfmobile.model.bean.Ligacao
import br.com.iresult.gmfmobile.ui.base.FormaEntregaDialog
import br.com.iresult.gmfmobile.ui.base.LoaderDialog
import br.com.iresult.gmfmobile.ui.base.PrintDialog
import br.com.iresult.gmfmobile.ui.base.SimpleDialog
import br.com.iresult.gmfmobile.ui.widget.HomeStatus
import br.com.iresult.gmfmobile.utils.addOnChangeListener
import br.com.iresult.gmfmobile.utils.defaultScheduler
import kotlinx.android.synthetic.main.dialog_entrega.*
import kotlinx.android.synthetic.main.fragment_leitura_menu.*
import net.danlew.android.joda.JodaTimeAndroid
import org.apache.commons.lang3.StringUtils
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.koin.androidx.viewmodel.ext.android.sharedViewModel


class LeituraMenuFragment : Fragment() {
    var contagem = 0
    val viewModel: LeituraViewModel by sharedViewModel()
    var qtdTentativas: Int = 0
    var imprimeOcorrenciaNotificacao = false
    var dialogProcess: AlertDialog? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        qtdTentativas = 0
        JodaTimeAndroid.init(activity);
        return inflater.inflate(R.layout.fragment_leitura_menu, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupCardClicks()

        leitura.addOnChangeListener {
            viewModel.setLeitura(it)
        }

        btClose.setOnClickListener {


            doAsync {

                var visita: Visita = Visita()
                if (!viewModel.leitura.value?.isEmpty()!!) {
                    visita = Visita(viewModel.leituraRepository.dataBase, viewModel.mUnidade.value, viewModel.leitura.value)
                    contagem++
                }

                uiThread {
                    viewModel.printInvoice(qtdTentativas, visita)

                }
            }
        }


        // val intent = Intent(context, RoteiroActivity::class.java).apply {
        //     putExtra("valCasas",contagem)
        //  }
        //  startActivity(intent)


        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)

        viewModel.unidade.observe(viewLifecycleOwner, Observer { unidade ->
            total_debitos.text = "0"
            checkButtonsAvailability(unidade)
        })

        viewModel.usuario.observe(viewLifecycleOwner, Observer { usuario ->
            total_debitos.text = "0"
            debitos.isEnabled = (usuario.debitos == "S")
            dados.isEnabled = (usuario.dadosCadastrais == "S")
            ordem.isEnabled = (usuario.os == "S")
        })

        viewModel.ocorrencia.observe(viewLifecycleOwner, Observer { oco ->
            Log.i("LeituraViewModel", "Campo de leitura sendo desabilitado")
            tvBadge.visibility = View.VISIBLE
            leitura.isEnabled = false
            NavHostFragment.findNavController(this).popBackStack()
        })

        viewModel.enableDisableJustify.observe(viewLifecycleOwner, Observer {
            Log.i("LeituraViewModel", "Verificando Ativação do campo Leitura, $it")
            leitura.isEnabled = !it
        })

        viewModel.leitura.observe(viewLifecycleOwner, Observer { leituraStr ->
            if (leitura.text.toString() != leituraStr) {
                leitura.setText(leituraStr)
            }
        })

        viewModel.deveHabilitarBotaoEnvio().observe(viewLifecycleOwner, Observer { habilitar ->
            btClose.isEnabled = habilitar
        })

        viewModel.isSegundaVia().observe(viewLifecycleOwner, Observer { habilitar ->

            btClose.setText("Imprimir Segunda Via")
        })


        viewModel.habilitarBotaoOcorrencia().observe(viewLifecycleOwner, Observer {
            //view.ocorrencia.isEnabled = it
        })

        viewModel.errorMessage.observe(viewLifecycleOwner, Observer { error ->
            val dialog = SimpleDialog(requireContext(), error)
            dialog.setupCloseButton(getString(R.string.ok))
            dialog.show()

        })

        viewModel.erroLeitura.observe(viewLifecycleOwner, Observer {
            val dialog = SimpleDialog(requireContext(), it)
            dialog.setupActionButton(getString(R.string.yes), getString(R.string.cancel)) {

                dialog.dismiss()

                var visita: Visita = Visita()
                if (!viewModel.leitura.value?.isEmpty()!!) {
                    visita = Visita(viewModel.leituraRepository.dataBase, viewModel.mUnidade.value, viewModel.leitura.value)

                }
                var notificacao = visita.ligacao?.numeroLigacao?.let { viewModel.leituraRepository.dataBase?.notificacaoDao()?.buscarNotificacao(it) }
                if (notificacao != null && !notificacao.isEmpty()) {
                    viewModel.mOcorrenciaNotificacaoMessage.value = "Deseja imprimir a notificacao ?"
                } else {
                    viewModel.setState(LeituraViewModel.State.IMPRESSAO)
                }
            }
            dialog.setDismissListener {

            }

            dialog.show()

        })


        viewModel.consumoZeroDialog.observe(viewLifecycleOwner, Observer {
            val dialog = SimpleDialog(requireContext(), it)
            dialog.setupActionButton(getString(R.string.yes), getString(R.string.cancel)) {

                dialog.dismiss()

                if (!viewModel.mUnidade.value!!.situacaoLigacao.equals("C") && !viewModel.mUnidade.value!!.situacaoLigacao.equals("K")) {

                    var ocor = viewModel.ocorrencia.value
                    if (viewModel.ocorrencia.value != null && ocor?.ocorrencia?.statusEmissao == "S") {
                        viewModel.mEnviarOcorrenciaGMF.postValue("ENVIAR")
                    }
                } else {
                    showCorteAPedido()
                }
            }
            dialog.setDismissListener {

            }

            dialog.show()

        })


        viewModel.ocorrenciaNotificacaoMessage.observe(viewLifecycleOwner, Observer {
            val dialog = SimpleDialog(requireContext(), it)
            dialog.setupActionButton(getString(br.com.iresult.gmfmobile.R.string.yes), getString(br.com.iresult.gmfmobile.R.string.no)) {

                dialog.dismiss()
                viewModel.mNotificacaoLeitura.value = "Ocorrencia de Notificação"

            }
            dialog.setDismissListener {
                imprimeOcorrenciaNotificacao = false;
                viewModel.setState(LeituraViewModel.State.IMPRESSAO)
            }

            dialog.show()

        })

        viewModel.notificacaoLeitura.observe(viewLifecycleOwner, Observer {

            val builder = AlertDialog.Builder(context)

            builder.setTitle(it)

            var notificacoes = viewModel.leituraRepository.dataBase?.ocorrenciaNotificacaoDao().findAll()

            var array = arrayOfNulls<String>(notificacoes.size)
            for ((idx, ocorNot) in notificacoes.withIndex()) {
                array[idx] = ocorNot.codOcorrencia + " - " + ocorNot.descricaoOcorrencia
            }
            var checkedItem = 0
            builder.setSingleChoiceItems(array, checkedItem) { dialog, which ->
                checkedItem = which
            }

            builder.setPositiveButton("OK") { dialog, which ->

                doAsync {

                    var leitura = viewModel.leituraRepository
                            .dataBase?.leituraDao().getLeitura(viewModel.mUnidade.value!!.numeroLigacao)
                            ?.defaultScheduler()?.blockingFirst()?.get(0)

                    leitura?.codEntregaNotificacao =
                            notificacoes.get(checkedItem).codOcorrencia
                    viewModel.leituraRepository
                            .dataBase?.leituraDao().updateLeitura(leitura!!);

                    imprimeOcorrenciaNotificacao = true;
                    viewModel.setState(LeituraViewModel.State.IMPRESSAO)
                }
            }

            builder.setNegativeButton("Cancel", null)

            val dialog = builder.create()
            dialog.show()

        })

        viewModel.confirmeLeituraMessage.observe(viewLifecycleOwner, Observer {

            val dialog = SimpleDialog(requireContext(), it)
            dialog.setupActionButton(getString(br.com.iresult.gmfmobile.R.string.yes), getString(br.com.iresult.gmfmobile.R.string.no)) {

                dialog.dismiss()
                viewModel.setState(LeituraViewModel.State.IMPRESSAO)

            }
            dialog.show()

        })

        viewModel.processDialog.observe(this, Observer {
            /*
                        val builder = AlertDialog.Builder(context)
                        builder.setMessage("Processando...")
                        dialogProcess =  builder.create()
                        dialogProcess?.show() */
        })

        viewModel.corteDialogMessage.observe(this, Observer {
            val dialog = SimpleDialog(requireContext(), it)
            dialog.setupCloseButton("OK")
            dialog.setDismissListener {
                doAsync {
                    val leitura = viewModel.leituraRepository.dataBase?.leituraDao()
                            ?.getLeitura(viewModel.mUnidade.value!!.numeroLigacao)?.defaultScheduler()?.blockingFirst()?.get(0)

                    val sync = SyncLeituraGMF()
                    sync.leituraRepository = viewModel.leituraRepository
                    sync.loginService = viewModel.loginService
                    sync.leituraGMFDao = viewModel.leituraRepository.dataBase.leituraGMFDao()

                    viewModel.mUnidade.value!!.status = HomeStatus.CONCLUIDO.name
                    viewModel.leituraRepository.dataBase.ligacaoDao().updateLigacao(viewModel.mUnidade.value!!).subscribe({ numberOfRows ->
                        viewModel.mUpdatedUnidade.postValue(viewModel.mUnidade.value!!)

                        val nomeArquivo = viewModel.nomeArquivo.value ?: ""
                        leitura?.let {
                            sync.enviarFotosGMF(leitura.numeroLigacao.toString(), leitura.sqLeitura.toString(), nomeArquivo)
                            sync.enviarFotosOcorrGMF(leitura.numeroLigacao.toString(), leitura.sqLeitura.toString(), nomeArquivo)
                        }

                        sync.enviarLeituraGMF(leitura!!, viewModel.mUnidade.value!!, viewModel.mErroDialogCloseMessage, viewModel.mUpdatedUnidade)
                    }, {
                        Log.e("LeituraViewModel", "Error to update $it")
                    })

                }
            }
            dialog.show()
        })


        viewModel.enviarOcorrenciaGMF.observe(this, Observer {

            doAsync {

                val leitura = viewModel.leituraRepository.dataBase?.leituraDao()
                        ?.getLeitura(viewModel.mUnidade.value?.numeroLigacao!!)?.defaultScheduler()?.blockingFirst()?.get(0)
                if (leitura?.statusTransmissao != 1) {
                    var sync = SyncLeituraGMF()
                    sync.leituraRepository = viewModel.leituraRepository
                    sync.loginService = viewModel.loginService
                    sync.leituraGMFDao = viewModel.leituraRepository.dataBase.leituraGMFDao()
                    sync.enviarLeituraGMF(leitura!!, viewModel.mUnidade.value!!, viewModel.mErroDialogCloseMessage, viewModel.mUpdatedUnidade)
                }
            }
        })

        viewModel.updatedUnidade.observe(this, Observer {
            activity?.setResult(Activity.RESULT_OK)
            activity?.onBackPressed()
        })



        viewModel.getState().observe(viewLifecycleOwner, Observer { state ->

            when (state) {

                LeituraViewModel.State.IMPRESSAO -> {

                    with(PrintDialog(requireContext())) {
                        setPrintListener {
                            //  viewModel.mProcessDialog.postValue("SHOW")
                            //TODO Deve imprimir a conta e assim que terminar mostrar dialog de forma de
                            //todo entrega da conta

                            if (!viewModel.isImpressoraConectada()) {
                                dismiss()
                                viewModel.mErrorMessage.value = "Impressora não configurada corretamente, favor verificar."
                            } else {
                                var lei = 0
                                if (StringUtils.isNotBlank(viewModel.leitura.value)) {
                                    lei = viewModel.leitura.value?.toInt()!!
                                }
                                val calculo = Calculo(viewModel.leituraRepository.dataBase,
                                        viewModel.leituraRepository,
                                        viewModel.loginService,
                                        viewModel.mUnidade.value!!,
                                        lei, viewModel.quantidadeEmissoes)
                                calculo.tiposOcorrencia = viewModel.tiposOcorrencia.value
                                if (viewModel.ocorrencia.value?.ocorrencia != null) {
                                    calculo.codOcorrencia = viewModel.ocorrencia.value?.ocorrencia?.codigo!!
                                }
                                viewModel.erroDialogCloseMessage.observe(viewLifecycleOwner, Observer { error ->
                                    val dialog = SimpleDialog(requireContext(), error)
                                    dialog.setupCloseButton(getString(R.string.ok))
                                    dialog.show()

                                    dialog.setDismissListener {
                                        activity?.setResult(Activity.RESULT_OK)
                                        activity?.onBackPressed()
                                    }
                                })
                                calculo.mUpdatedUnidade = viewModel.mUpdatedUnidade
                                calculo.imprimeOcorrenciaNotificacao = imprimeOcorrenciaNotificacao

                                calculo.calculaConta()
                                dismiss()

                                with(LoaderDialog(requireContext(), "Imprimindo...")) {

                                    show()

                                    calculo.mContaCorteDialog.observe(viewLifecycleOwner, Observer {
                                        dismiss()
                                        showCorteAPedido()
                                    })

                                    calculo.showFormaDeEntregaDialog.observe(viewLifecycleOwner, Observer {
                                        dismiss()
                                        FormaEntregaDialog(requireContext()).let { deliveryDialog ->
                                            deliveryDialog.setSaveListener {
                                                deliveryDialog.dismiss()
                                                viewModel.saveUnidade(it, deliveryDialog.tvMotivo.text.toString())
                                            }
                                        }.show()
                                    })

                                    calculo.showContaRetidaDialog.observe(viewLifecycleOwner, Observer {
                                        dismiss()
                                        SimpleDialog(requireContext(), it)
                                                .setDismissListener {
                                                    viewModel.saveUnidade(FormaEntregaDialog.FormaEntrega.FORMA_ENTREGA_NAO_ENTREGUE, it)
                                                }
                                                .show()
                                    })
                                }
                            }
                        }.show()
                    }
                }
            }
        })
    }

    private fun showCorteAPedido() {

        var msg = " - NÃO EXISTE IMPRESSÃO, OS DADOS SERÃO ENVIADOS PARA A GMF"
        msg = if (viewModel.mUnidade.value!!.situacaoLigacao.equals("C")) {
            "LIGACAO CORTADA $msg"
        } else {
            "LIGACAO COM CORTE A PEDIDO $msg"
        }
        viewModel.mCorteDialogMessage.postValue(msg)
    }

    private fun setupCardClicks() {
        debitos.setOnClickListener {
            NavHostFragment.findNavController(this).navigate(R.id.debitos)
        }

        dicas.setOnClickListener {
            NavHostFragment.findNavController(this).navigate(R.id.dicas)
        }

        ordem.setOnClickListener {
            NavHostFragment.findNavController(this).navigate(R.id.ordemServico)
        }

        ocorrencia.setOnClickListener {
            NavHostFragment.findNavController(this).navigate(R.id.ocorrencia)
        }

        dados.setOnClickListener {
            NavHostFragment.findNavController(this).navigate(R.id.dadosCadastrais)
        }

//        mapa.setOnClickListener {
//            this.navigateToMapHome()
//        }

        observacao.setOnClickListener {
            NavHostFragment.findNavController(this).navigate(R.id.observacao)
        }

        foto.setOnClickListener {
            NavHostFragment.findNavController(this).navigate(R.id.foto)
        }
    }

    private fun checkButtonsAvailability(lig: Ligacao) {
        // val activeButtonBackGround = R.drawable.leitura_menu_item_bg
        // val disabledButtonBackGround = TypedValue()
        // context?.getTheme()?.resolveAttribute(R.attr.colorPrimary, activeButtonBackGround, true)
        // context?.getTheme()?.resolveAttribute(R.attr.colorPrimary, disabledButtonBackGround, true)
        // var backColor : Int = Color.BLACK
        var enabled = true
        var textVisible = View.VISIBLE
        if (lig.status != null
                && lig.status != HomeStatus.AGORA.name
                && lig.status != HomeStatus.PROXIMO.name) {
            // Residencia está concluida
            Log.i("LeituraActivity", "Ligação está Concluida")
            //backColor = getResources().get.getColor(attr/colorPrimary);Color.LTGRAY
            // backColor = disabledButtonBackGround.data
            enabled = false
            textVisible = View.GONE
        } else {
            // Residencia ainda pendente
            Log.i("LeituraActivity", "Ligação ainda está Pendente")
        }
        ocorrencia?.isEnabled = enabled
        dicas?.isEnabled = enabled
        observacao?.isEnabled = enabled
        leitura?.isEnabled = enabled
        foto?.isEnabled = enabled
        // leitura?.visibility = textVisible
        ocorrencia?.setBackgroundResource(R.drawable.leitura_menu_item_bg)
        dicas?.setBackgroundResource(R.drawable.leitura_menu_item_bg)
        observacao?.setBackgroundResource(R.drawable.leitura_menu_item_bg)
    }


//    private fun navigateToMapHome() {
//        val ligacao: Ligacao = activity?.intent?.extras?.getParcelable(LeituraActivity.ARG_UNIDADE)
//                ?: return
//        context?.startActivity<MapHomeActivity>(MapHomeActivity.EXTRA_LIGACAO to ligacao)
//    }
}